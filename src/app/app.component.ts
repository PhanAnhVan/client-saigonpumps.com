import { Component, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Meta, Title } from '@angular/platform-browser';
import { Globals } from './globals';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
    public connect;
    public token = {
        getcompany: "api/company"
    }
    constructor(
        private translate: TranslateService,
        private globals: Globals,
        private title: Title,
        private meta: Meta,
    ) {
        this.globals.configCKeditor = Object.assign(this.globals.configCKeditor, this.globals.configUploadCkeditor);
        this.globals.send({ path: this.token.getcompany, token: 'getcompany' })
        this.translate.setDefaultLang('vi');
        this.translate.use('vi');
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case 'getcompany':
                    let data = response.data;
                    this.globals.company = data;

                    this.title.setTitle(data.name);
                    this.meta.addTag({ name: 'description', content: data.description });
                    this.meta.addTag({ name: 'keywords', content: data.keywords });
                    let shortcut = document.querySelector("[type='image/x-icon']");
                    if (shortcut && data.shortcut) { shortcut.setAttribute('href', data.shortcut); }
                    break;
                default:
                    break;
            }
        })
    }

    ngOnDestroy() { this.connect.unsubscribe(); }
}
