import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Globals } from "../globals";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit, OnDestroy {
    public connect;
    public hide = true;
    public type = "password";
    public token: any = {
        loginadmin: "api/login/admin",
    };
    public fm: FormGroup;
    public company: any;
    constructor(
        private router: Router,
        public formBuilder: FormBuilder,
        public translate: TranslateService,
        public toastr: ToastrService,
        private globals: Globals
    ) {
        this.fm = formBuilder.group({
            email: ["", [Validators.required, Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i),],],
            password: ["", [Validators.required, Validators.minLength(8)]],
        });
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "loginadmin":
                    let type = res["status"] == 1 ? "success" : res["status"] == 0 ? "warning" : "danger";
                    this.toastr[type](res['message'], type, { timeOut: 1500 });

                    if (+res.status === 1) {
                        setTimeout(() => {
                            this.globals.USERS.set(res.data, true);
                            this.router.navigate([this.globals.admin + "/dashboard"]);
                        }, 500);
                    }
                    break;
                default:
                    break;
            }
        });
    }
    ngOnInit() {
        if (this.globals.USERS.check(true)) {
            this.router.navigate([this.globals.admin + "/dashboard"]);
        } else {
            this.globals.USERS.remove();
        }
    }
    ngAfterViewInit() {
        setTimeout(() => {
            let el = document.getElementById("facebook");
            if (el) {
                el.remove();
            }
        }, 1000);
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    onSubmit() {
        let data = this.fm.value;
        this.globals.send({ path: this.token.loginadmin, token: "loginadmin", data: data, });
    }
}
