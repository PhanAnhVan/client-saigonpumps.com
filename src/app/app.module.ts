import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';

import { Globals } from './globals';
import { AdminAuthGuard } from './services/auth/adminAuth.guard';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';

const appRoutes = [
    { path: 'admin', loadChildren: () => import('./backend/backend.module').then(m => m.BackendModule), canActivate: [AdminAuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: '', loadChildren: () => import('./frontend/frontend.module').then(m => m.FrontendModule) },
]

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules }),
        TranslateModule.forRoot({
            loader: { provide: TranslateLoader, useFactory: HttpLoaderFactory, deps: [HttpClient] }
        }),
    ],
    providers: [
        Globals,
        AdminAuthGuard
    ],
    bootstrap: [
        AppComponent
    ],
})
export class AppModule { }
