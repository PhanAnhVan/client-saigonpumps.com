import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { OwlOptions, SlidesOutputData } from "ngx-owl-carousel-o";
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Globals } from '../../../globals';
import { CartService } from '../../../services/apicart/cart.service';

@Component({
    selector: 'app-detail-product',
    templateUrl: './detail-product.component.html',
    styleUrls: ['./detail-product.component.scss'],
    providers: [CartService]
})
export class DetailProductComponent implements OnInit, OnDestroy {
    public connect: Subscription;
    public data: any = {};
    public price: number = 0;
    public link: any;
    public activeSlides: SlidesOutputData;
    imgListLength: number;
    @ViewChild('owlLibrary', { static: false }) owlLibrary: any;

    public token: any = {
        getProductDetail: "api/products/detail",
    }

    constructor(
        public route: ActivatedRoute,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals, public apiCart: CartService,
        private title: Title,
        private meta: Meta
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getProductDetail":
                    this.data = res.data;
                    this.cart.amount = 1;

                    this.title.setTitle(this.data.name);
                    this.meta.updateTag({ name: 'description', content: this.data.description });
                    this.meta.updateTag({ name: 'keywords', content: this.data.keywords });

                    if (Object.keys(this.data).length > 0) {
                        let listimages = (res.data.listimages && res.data.listimages.length > 5) ? JSON.parse(res.data.listimages) : [];
                        this.listImages.cached = listimages;
                        if (this.data.images && this.data.images.length > 4) {
                            this.listImages.cached = [this.data.images].concat(listimages);
                        } else {
                            this.data.images = (listimages.length > 0) ? listimages[0] : '';
                        }
                        this.listImages.data = Object.values(this.listImages.cached);

                        this.price = (this.data.price_sale && +this.data.price_sale != 0) ? this.data.price_sale : this.data.price
                        this.price = +this.data.min > 0 ? 0 : this.price;

                        let attribute = [];
                        let data = []
                        if (this.data.priceattr && this.data.priceattr.length > 0) {
                            this.processAttr.list = this.data.priceattr;
                            this.data.priceattr.filter((item: any) => {
                                if (item.images != '') {
                                    this.listImages.data.push(item.images);
                                    item.idimages = this.listImages.data.length - 1
                                }
                                item.att_id_value = JSON.parse(item.att_id_value);
                                if (item.att_id_value && item.att_id_value.length > 0) {
                                    data = data.concat(item.att_id_value)
                                }
                                if (item.list && item.list.length > 0) {
                                    item.list.forEach(element => {
                                        if (!attribute[element.parent_id]) {
                                            attribute[element.parent_id] = { id: element.parent_id, name: element.parent_name, data: [], active: 0 }
                                        }
                                        attribute[element.parent_id].data[element.attribute_id] = { id: element.attribute_id, name: element.name }
                                    });
                                }
                            })
                        }

                        this.processAttr.lisActvie = data.reduce((n, o, i) => { n[o] = o; return n }, []);

                        attribute.filter((item: any) => item.data = Object.values(item.data));

                        this.data.attribute = Object.values(attribute);
                        // sanphamlienquan
                        this.data.related = res.data.related;

                        setTimeout(() => { this.renderHtml(); }, 500);

                        this.cart.data = {

                            'id_products': this.data.id,

                            'path': this.data.pages_link + '/' + this.data.link,

                            'name': this.data.name,

                            'images': this.globals.BASE_API_URL + 'public/products/' + this.data.images,

                            'price': this.price,

                            attribute: [],

                            amount: 1

                        }
                    } else {
                        this.router.navigate['/404']
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.link = params.link || '';

            this.globals.send({ path: this.token.getProductDetail, token: "getProductDetail", params: { link: this.link } });
        })
    }

    isActive(images: any, index: number) {
        this.data.images = images;
        this.listImages.active = index;
    }

    ngOnDestroy() { this.connect.unsubscribe(); }

    renderHtml = () => {
        let main = document.getElementById("contentDetail");
        if (main) {
            let el = main.querySelectorAll("table");
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement("div");
                    div.className = "table-responsive table-bordered m-0 border-0";
                    el[i].parentNode.insertBefore(div, el[i]);
                    el[i].className = 'table';
                    el[i].setAttribute('class', 'table');
                    let cls = el[i].getAttribute('class');
                    el[i]
                    let newhtml = "<table class='table'>" + el[i].innerHTML + "</table>";
                    el[i].remove();
                    div.innerHTML = newhtml;
                }
            }
            let image = main.querySelectorAll("img");
            if (image) {
                for (let i = 0; i < image.length; i++) {
                    let a = document.createElement("div");
                    a.className = "images-detail d-inline";
                    image[i].parentNode.insertBefore(a, image[i]);
                    let src = image[i].currentSrc;
                    let html = `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` + src + `">
                        `+ image[i].outerHTML + `
                    </a>`
                    image[i].remove()
                    a.innerHTML = html;
                }
            }
        }
    }

    public listImages = {
        data: [],
        cached: [],
        active: 0,
        options: <OwlOptions>{
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: true,
            dots: false,
            navSpeed: 500,
            items: 5,
            navText: [
                "<img src='../../../../../assets/img/left-arrow.png' width='20' height='100%'>",
                "<img src='../../../../../assets/img/right-arrow.png' width='20' height='100%'>"
            ],
            nav: false,
        }
    };

    public library = {
        libraryOptions: <OwlOptions>{
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            dots: false,
            navSpeed: 500,
            items: 1,
            navText: [
                "<img src='../../../../../assets/img/left-arrow.png' width='20' height='100%'>",
                "<img src='../../../../../assets/img/right-arrow.png' width='20' height='100%'>"
            ],
            nav: true,
        },

        getPassedData: (data: SlidesOutputData) => {
            this.activeSlides = data;
            let selected = data.slides[0] ? data.slides[0].id : "";
            this.data.images = selected;
            setTimeout(() => {
                this.library.onClickImageChild(selected);
            }, 200);
        },

        onClickImageChild: (index) => {
            let id = index + '-child';
            let items: any = document.querySelectorAll('.img-child');
            if (items.length > 0) {
                for (let i = 0; i < items.length; i++) {
                    items[i].style.opacity = 0.6;
                    items[i].classList.remove("img_active");
                }
            }
            if (document.getElementById(id)) {
                document.getElementById(id).style.opacity = '1';
                document.getElementById(id).classList.add("img_active");
            }
            this.owlLibrary.to(index);
        }
    };

    cart = {

        amount: 1,

        data: <any>{},

        changeAmount: () => {
            if (this.cart.amount <= 0 || !Number.isInteger(this.cart.amount)) {
                this.cart.amount = 1;
            }
        },

        addCart: () => {
            this.cart.data.id = this.data.id;
            this.cart.data.price = this.price;
            this.cart.data.amount = this.cart.amount;
            this.cart.data.attribute = this.processAttr.item.list
            let data = this.apiCart.reduce();
            if (data[this.cart.data.id]) {
                let a = isNaN(+data[this.cart.data.id].amount) ? 1 : +data[this.cart.data.id].amount;
                this.cart.data.amount = +a + this.cart.amount;
            }
            this.apiCart.edit(this.cart.data, this.cart.data.id);

            const notification = document.getElementById('notification');

            window.scroll({ top: notification.offsetTop - 50, behavior: 'smooth' });

            notification.classList.add('d-block');

            setTimeout(() => {
                notification.classList.remove('d-block');
            }, 3e3);
        },
    }

    processAttr = {
        list: [],
        lisActvie: [],
        listAttri: [],
        item: <any>{},
        _active: (attribute, attr_id) => {
            attribute.active = attr_id;
            let data = attribute.data.map((obj) => obj.id) || []
            this.processAttr.list.filter(item => {
                if (item.att_id_value && item.att_id_value.length > 0) {
                    if (item.att_id_value.indexOf(attr_id) !== -1) {
                        data = data.concat(item.att_id_value)
                    }
                }
            });

            this.processAttr.lisActvie = data.reduce((n, o, i) => { n[o] = o; return n }, []);

            this.processAttr.listAttri = [];
            this.data.attribute.filter(item => item.active > 0 ? this.processAttr.listAttri[item.active] = item.active : '');
            if (this.processAttr.list && this.processAttr.list.length > 0) {
                let item = this.processAttr.list.find(item => {
                    let skip = this.processAttr._check(item.att_id_value, this.processAttr.listAttri);
                    return skip
                })
                this.processAttr.item = item || {};
                if (Object.values(this.processAttr.item).length > 0) {
                    this.price = this.processAttr.item.price;
                    this.processAttr.item.idimages > 0 ? this.library.onClickImageChild('images-' + this.processAttr.item.idimages) : false;
                }
            }
        },

        _check: (array1, array2) => {
            return Object.values(array1).length === Object.values(array2).length && array1.sort().every(function (value, index) { return value === array2.sort()[index] });
        }
    }
}
