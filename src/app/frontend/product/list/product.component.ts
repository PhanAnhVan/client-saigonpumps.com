import { Component, Input, OnChanges, OnDestroy, OnInit } from "@angular/core";
import { Globals } from "../../../globals";
import { ActivatedRoute, Router } from "@angular/router";
import { TableService } from "../../../services/integrated/table.service";
import { PageChangedEvent } from "ngx-bootstrap/pagination";
import { ToslugService } from '../../../services/integrated/toslug.service';
import { LabelType, ChangeContext, Options } from 'ng5-slider';

@Component({
    selector: "app-product-list",
    templateUrl: "./product.component.html",
    styleUrls: ["./product.component.css"],
    providers: [ToslugService]

})
export class ListProductComponent implements OnInit, OnChanges, OnDestroy {
    @Input('link') link: string = '';

    public connect;
    public cwstable = new TableService();
    public show: number = -1;
    public price: any = { min: 0, max: 0 };
    public data: any = {};
    public categories: any = [];
    public cateListId = [];
    public width = document.body.getBoundingClientRect().width;
    public collapsedMenu = [];
    public openMobileFilter = false;
    public minValue: number = 0;
    public maxValue: number = 100;
    public token = {
        getProduct: "api/products/list",
        getCategory: "api/products/getCategory",
    };

    private cols = [
        { title: 'FEProduct.sortName', field: 'name', filter: true, active: true },
        { title: 'FEProduct.sortPrice', field: 'price_filter', filter: true, type: 'number' },
    ];

    public options: Options = {
        floor: 0,
        ceil: 100,
        translate: (value: number, label: LabelType): string => {
            switch (label) {
                case LabelType.Low:
                    return '<div class="d-inline-block" style="font-size:11px">Min: ' + this.setnumber(value) + ' đ </div>';
                case LabelType.High:
                    return '<b style="font-size:11px">Max: ' + this.setnumber(value) + ' đ </b>'
                default:
                    return this.setnumber(value);
            }
        }
    };

    constructor(
        public globals: Globals,
        public route: ActivatedRoute,
        public router: Router,
        public toSlug: ToslugService
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {

                case "getCategory":
                    this.categories = res.data;
                    this.cateListId = res.data.reduce((n, o) => {
                        n[o.id] = o.list;
                        return n;
                    }, []);
                    break;

                case "getProduct":
                    this.data = [];
                    this.data = res.data;
                    this.show = this.data.list && this.data.list.length > 0 ? 1 : 0;
                    this.data.list.reduce((n, o, i) => {
                        o.price_filter = +o.price_sale && +o.price_sale > 0 ? +o.price_sale : +o.price;
                        return n;
                    }, [])
                    this.cwstable._concat(this.data.list, true);
                    this.extract();
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getCategory, token: "getCategory", });
        this.cwstable._ini({
            data: [], cols: this.cols, keyword: "getProduct", count: this.Pagination.itemsPerPage, sorting: { field: "name", sort: "", type: "number" },
        });
        // if (this.link && this.link != '') {
        //     this.globals.send({ path: this.token.getProduct, token: 'getProduct', params: { link: this.link } });
        // }
    }

    ngOnChanges() {
        if (this.link && this.link != '') {
            this.globals.send({ path: this.token.getProduct, token: 'getProduct', params: { link: this.link } });

        }
    }

    ngOnDestroy() { this.connect.unsubscribe(); }

    public Pagination = {
        maxSize: 4,
        itemsPerPage: 28,
        change: (event: PageChangedEvent) => {
            const startItem = (event.page - 1) * event.itemsPerPage;
            const endItem = event.page * event.itemsPerPage;
            this.cwstable.data = this.cwstable.cached.slice(startItem, endItem);
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    }

    public brand = {
        data: [],
        value: [],
        filterBrand: (id) => {
            let token = "brand_id";
            let filter = this.cwstable._getFilter(token);
            let data = !filter.value ? {} : filter.value.reduce((n, o, i) => { n[o] = o; return n; }, {});
            if (data[id]) {
                this.brand.value = filter.value.filter(item => { return +item == id ? false : true; });
            } else {
                this.brand.value.push(id);
            }
            if (this.brand.value.length == 0) {
                this.cwstable._delFilter(token);
            } else {
                this.cwstable._setFilter(token, this.brand.value, 'in');
            }
        }
    }

    public origin = {
        data: [],
        value: [],
        filterOrigin: (id) => {
            let token = "origin_id";
            let filter = this.cwstable._getFilter(token);
            let data = !filter.value ? {} : filter.value.reduce((n, o, i) => { n[o] = o; return n; }, {});
            if (data[id]) {
                this.origin.value = filter.value.filter(item => { return +item == id ? false : true; });
            } else {
                this.origin.value.push(id);
            }
            if (this.origin.value.length == 0) {
                this.cwstable._delFilter(token);
            } else {
                this.cwstable._setFilter(token, this.origin.value, 'in');
            }
        }
    }

    public groupCategory = {
        data: [],
        value: [],

        filter: (id, skip = 0) => {
            let token = "page_id";
            let filter = this.cwstable._getFilter(token);
            let data = !filter.value ? {} : filter.value.reduce((n, o) => {
                n[o] = o;
                return n;
            }, {});

            if (data[id]) {
                this.groupCategory.value = filter.value.filter(item => {
                    if (skip == 1) {
                        return true;
                    } else {
                        return +item == id ? false : true;
                    }
                });
            } else {
                this.groupCategory.value.push(id);
            }

            if (this.groupCategory.value.length == 0) {
                this.cwstable._delFilter(token);
            } else {
                this.cwstable._setFilter(token, this.groupCategory.value, 'in');
            }
        },
    }

    extract = () => {
        let brand = {};
        let origin = {};
        let price = { min: 0, max: 0 };
        if (this.cwstable.cachedList.length > 0) {
            price.min = (+this.cwstable.cachedList[0].price_sale > 0) ? +this.cwstable.cachedList[0].price_sale : +this.cwstable.cachedList[0].price;
        }

        this.cwstable.cachedList.reduce((n, o, i) => {

            if (o.brand_id && +o.brand_id > 0) {
                if (!brand[o.brand_id]) {
                    brand[o.brand_id] = { id: o.brand_id, name: o.brand_name, count: 0 };
                }
                if (brand[o.brand_id]) {
                    brand[o.brand_id].count = +brand[o.brand_id].count + 1;
                }
            }

            if (o.origin_id && +o.origin_id > 0) {
                if (!origin[o.origin_id]) {
                    origin[o.origin_id] = { id: o.origin_id, name: o.origin_name, count: 0 };
                }
                if (origin[o.origin_id]) {
                    origin[o.origin_id].count = +origin[o.origin_id].count + 1;
                }
            }

            // if (o.page_id && +o.page_id > 0) {
            //     if (!group[o.page_id]) {
            //         group[o.page_id] = { id: o.page_id, name: o.parent_name, count: 0 };
            //     }
            //     if (group[o.page_id]) {
            //         group[o.page_id].count = +group[o.page_id].count + 1;
            //     }
            // }

            let p = (+o.price_sale > 0) ? +o.price_sale : +o.price;
            price.min = p < price.min ? p : price.min;
            price.max = p > price.max ? p : price.max;

            this.changeOptions({
                floor: 0,
                ceil: price.max
            })

            return n;
        }, {});

        this.brand.data = Object.values(brand);
        this.origin.data = Object.values(origin);
        this.price = price;
    }

    changeOptions(option) {

        const newOptions: Options = Object.assign({}, this.options);

        newOptions.ceil = option.ceil;

        newOptions.floor = option.floor;

        this.options = newOptions;

        this.minValue = option.floor;

        this.maxValue = option.ceil;
    }

    onUserChangeEnd(changeContext: ChangeContext): void {
        this.cwstable._setFilter('price', [changeContext.value, changeContext.highValue], 'between');
    }

    setnumber(Val) {

        let a = new Number(Val);

        return a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    }

    collapseMenu(id) {
        if (this.collapsedMenu.includes(id) === true) {
            var index = this.collapsedMenu.indexOf(id);
            this.collapsedMenu.splice(index, 1);
        } else {
            this.collapsedMenu.push(id);
        }
    }

    setMenuMobile(open) {
        let list = document.getElementsByTagName('body')[0];
        this.openMobileFilter = open;
        list.style.position = open ? 'fixed' : 'inherit';
    }
}