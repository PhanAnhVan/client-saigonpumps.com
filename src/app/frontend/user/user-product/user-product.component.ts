import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../globals';
import { TableService } from '../../../services/integrated/table.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/public_api';

@Component({
    selector: 'app-user-product',
    templateUrl: './user-product.component.html',
    styleUrls: ['./user-product.component.scss']
})
export class UserProductComponent implements OnInit, OnDestroy {

    public info: any = {};

    public connect: any;

    private cols: any = [
        { title: 'FECustomer.order', field: 'order_code', show: true, filter: true, type: 'text' },
        { title: 'FECustomer.product_name', field: 'name', show: true, filter: true, type: 'text' },
        // { title: 'FECustomer.service', field: 'service_name', show: true, filter: true, type: 'text' },
        { title: 'FECustomer.countDocument', field: 'count_document', show: true, filter: true, type: 'number' },
        { title: 'FECustomer.guarantee', field: 'guarantee', show: true, filter: true, type: 'text' },
        { title: 'FECustomer.lblMaker_date', field: 'maker_date', show: true, filter: true, type: 'date' },
        { title: '#', field: 'action', show: true },
    ]

    constructor(
        public globals: Globals,
        public toastr: ToastrService
    ) {
        this.info = this.globals.CUSTOMER.get();

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getListProductsCustomer':
                    this.product.cws._concat(res.data, true);
                    break
                default:
                    break;
            }
        })
    }

    ngOnInit(): void {
        this.product.cws._ini({ cols: this.cols, data: [], keyword: 'user-product', sorting: { field: "maker_date", sort: "DESC", type: "date" } });
        this.product.send();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    public Pagination = {
        maxSize: 4,
        itemsPerPage: 28,
        change: (event: PageChangedEvent) => {
            const startItem = (event.page - 1) * event.itemsPerPage;
            const endItem = event.page * event.itemsPerPage;
            this.product.cws.data = this.product.cws.cached.slice(startItem, endItem);

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    }

    public product = {
        token: 'api/customer/getListProducts',
        cws: new TableService(),
        send: () => {
            this.globals.send({ path: this.product.token, token: 'getListProductsCustomer', params: { id: this.info.id } });
        }
    }
}
