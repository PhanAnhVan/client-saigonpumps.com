import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { TableService } from 'src/app/services/integrated/table.service';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-user-porduct-detail',
    templateUrl: './user-product-detail.component.html',
    styleUrls: ['./user-product-detail.component.scss']
})
export class UserProductDetailComponent implements OnInit, OnDestroy {
    public connect: Subscription;
    public info: { id: any; };
    public order_detail_id: any;
    public item: any;
    public modalRef: BsModalRef<Object>;
    public isClicked: boolean;

    public cols = [
        { title: "lblStt", field: 'index', show: true, filter: true },
        { title: "FECustomer.nameDocument", field: 'name', show: true, filter: true, type: 'text' },
        { title: "#", field: 'action', show: true },
    ]

    constructor(
        public globals: Globals,
        public toastr: ToastrService,
        private routerAct: ActivatedRoute,
        private modalService: BsModalService
    ) {
        this.info = this.globals.CUSTOMER.get();
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'productsDetail':
                    this.item = res.data;
                    this.productDetail.cws._concat(res.data.document_customer, true);
                    break;
                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.routerAct.params.subscribe(params => {
            this.order_detail_id = params.order_detail_id;
            this.productDetail.send();
        });
        this.productDetail.cws._ini({
            cols: this.cols,
            data: [],
            keyword: 'contact', sorting: { field: "maker_date", sort: "DESC", type: "date" }
        });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    formatPrice(number: { toString: () => string; }) {
        number = number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
        return number;
    }

    /*
    openModal(template: TemplateRef<any>, item: any) {
        this.modalRef = this.modalService.show(template);
        this.itemDocument = item;
    }
    */

    public productDetail = {
        token: 'api/customer/getDetailProduct',
        cws: new TableService(),
        send: () => {
            this.globals.send({
                path: this.productDetail.token,
                token: 'productsDetail',
                params: {
                    order_detail_id: this.order_detail_id,
                    id: this.info.id
                }
            });
        }
    }
}
