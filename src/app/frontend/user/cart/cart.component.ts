import { Component, OnInit, OnDestroy, SimpleChanges, OnChanges, Input } from '@angular/core';
import { TableService } from '../../../services/integrated/table.service';
import { ActivatedRoute } from '@angular/router';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-infocart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class InfoCartComponent implements OnInit, OnChanges, OnDestroy {
    private cols = [
        { title: 'FECart.code', field: 'code', show: true, filter: true },
        { title: 'FECart.dayStart', field: 'day_start', show: true, filter: true },
        { title: 'FECart.priceTotal', field: 'price_total', show: true, filter: true },
        { title: 'FECart.status', field: 'delivery_status', show: true, filter: true },
    ];
    public cwstable = new TableService();
    public connect;
    public token: any = {
        listorders: "api/customer/orders"
    }
    constructor(public globals: Globals) {
        this.globals.send({ path: this.token.listorders, token: 'listorders', params: { id: this.globals.CUSTOMER.get().id || 0 } });

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'listorders':
                    this.cwstable._concat(response.data, true);
                    break;
                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'content' });

    }
    ngOnChanges(changes: SimpleChanges): void { }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }

}
