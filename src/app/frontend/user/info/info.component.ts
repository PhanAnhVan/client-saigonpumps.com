import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Globals } from '../../../globals';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-info-user',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit, OnDestroy {

    @Input('id') id: number;
    fm: FormGroup;
    public connect: any;
    public phone: number;
    public info: any = {}
    public token: any = {
        update: "api/customer/update",
    }

    constructor(
        public fb: FormBuilder,
        public router: Router,
        public globals: Globals,
        public translate: TranslateService,
        private toastr: ToastrService,
    ) {
        this.info = this.globals.CUSTOMER.get();

        Object.keys(this.info).length > 0 ? this.fmConfigs(this.info) : this.fmConfigs();

        this.connect = this.globals.result.subscribe((response: any) => {

            switch (response['token']) {
                case 'updateCustomer':
                    let type = (response['status'] == 1) ? "success" : (response['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](response['message'], type, { timeOut: 1500 });
                    if (response['status'] == 1) {
                        this.globals.CUSTOMER.set(response['data']);
                    }
                    break;

                default:
                    break;
            }
        })
    }

    ngOnInit() { }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    fmConfigs(item: any = "") {
        item = typeof item === 'object' ? item : { type: 1, sex: 1, };
        this.fm = this.fb.group({
            sex: +item.sex,
            email: [item.email ? item.email : '', [Validators.required, Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)]],
            phone: [item.phone ? item.phone : '', [Validators.pattern(/[0-9\+\-\ ]/)]],
            address: item.address ? item.address : '',
            name: [item.name ? item.name : '', [Validators.required]],
            id: +item.id ? +item.id : 0,
        })
    }

    onSubmit() {
        let data = this.fm.value;
        data.status = 1;
        this.globals.send({ path: this.token.update, token: 'updateCustomer', data: data });
    }
}
