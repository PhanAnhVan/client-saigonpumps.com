import { Component, OnDestroy, OnInit } from "@angular/core";
import { Meta, Title } from "@angular/platform-browser";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import * as AOS from 'aos';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Globals } from "src/app/globals";

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit, OnDestroy {
    private connect;

    width: number = window.innerWidth;
    public modalRef: BsModalRef;
    public skip: boolean = false;
    public titleBoxHomePage = 0;

    private token = {
        slide: 'api/home/slide',
        product: 'api/home/getProduct',
        about: 'api/home/aboutUs',
        aboutService: 'api/home/getServiceFooter',
        service: 'api/home/getService',
        news: 'api/home/content',
        partner: 'api/home/getPartner'
    }
    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,
        public modalService: BsModalService,
        private title: Title,
        private meta: Meta
    ) {
        this.title.setTitle(this.globals.company.name);
        this.meta.updateTag({ name: 'description', content: this.globals.company.description });

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getSlide':
                    this.slide.data = res.data
                    if (this.slide.data?.length > 0) {
                        this.slide.options.dots === true
                        this.slide.options.mouseDrag === true
                    }
                    break

                case 'getHomeAboutUs':
                    this.about.data = res.data;
                    break;

                case 'getAboutService':
                    this.about.service = res.data;
                    break;

                case "getProductHot":
                    this.product.hot = res.data;
                    if (this.width < 768) {
                        this.product.mobileHot = this.product.compaidData(res.data);
                    }
                    break;

                case "getServiceHome":
                    this.service.data = res.data;
                    break;

                case 'getHomeContent':
                    this.news.data = res.data
                    break;

                case "getPartner":
                    this.partner.list = res.data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        // config animation AOS
        AOS.init({ once: true });

        this.slide.send();
        this.about.send();
        this.aboutService.send();
        this.service.send();
        this.product.send();
        this.news.send();
        this.partner.send();
    }

    ngOnDestroy() { this.connect.unsubscribe() }

    public slide = {
        data: [],
        send: () => {
            this.globals.send({
                path: this.token.slide,
                token: 'getSlide',
                params: { type: 1 }
            })
        },
        options: {
            loop: true,
            mouseDrag: false,
            touchDrag: true,
            pullDrag: false,
            dots: false,
            navSpeed: 500,
            autoplayTimeout: 10000,
            autoplaySpeed: 1000,
            autoplay: false,
            items: 1,
            nav: true,
            navText: [
                '<img width="100%" height="100%" src="../../../../assets/img/icon-slide-left.png" alt="Arrow left" lazyload" />',
                '<img width="100%" height="100%" src="../../../../assets/img/icon-slide-right.png" alt="Arrow right" lazyload" />',
            ],
        },
    }

    public about = {
        data: <any>{},
        service: [],
        send: () => {
            this.globals.send({
                path: this.token.about,
                token: 'getHomeAboutUs'
            })
        }
    }

    public aboutService = {
        send: () => {
            this.globals.send({
                path: this.token.aboutService,
                token: 'getAboutService'
            })
        }
    }

    public product = {
        hot: [],
        mobileHot: [],
        send: () => {
            this.globals.send({
                path: this.token.product,
                token: "getProductHot",
                params: {
                    type: "hot",
                    limit: 10
                }
            });
        },
        compaidData: (data) => {
            let list = [];
            if (data && data.length > 0) {
                let length = data.length / 2;
                for (let i = 0; i < length; i++) {
                    list[i] = [];
                    if (list[i].length < 2) {
                        list[i] = data.splice(0, 2);
                    }
                    data = Object.values(data);
                }
            }
            return list;
        },
        options: {
            loop: false,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            autoplay: true,
            stagePadding: 10,
            margin: 10,
            responsive: {
                0: {
                    items: 2
                },
                768: {
                    items: 3
                },
                1024: {
                    items: 6
                },
            },
            dots: false,
            nav: true,
            navText: [
                '<img src="../../../../assets/img/left-arrow.png" alt="Left" width="100%" height="100%" />',
                '<img src="../../../../assets/img/right-arrow.png" alt="Right" width="100%" height="100%" />',
            ],
        },
    }

    public service = {
        data: [],
        send: () => {
            this.globals.send({
                path: this.token.service,
                token: "getServiceHome"
            })
        },
        options: {
            loop: true,
            nav: false,
            stagePadding: 1,
            responsive: {
                0: {
                    items: 1,
                    dots: true,
                    touchDrag: true,
                },
                768: {
                    items: 2
                },
                1025: {
                    items: 3,
                    margin: 30,
                    dots: false,
                    mouseDrag: false,
                }
            },
        }
    }

    public news = {
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.token.news,
                token: 'getHomeContent'
            })
        },
        options: {
            loop: true,
            stagePadding: 5,
            margin: 30,
            dots: true,
            touchDrag: true,
            nav: true,
            autoplay: true,
            navText: [
                '<img src="../../../../assets/img/icon-slide-left.png" alt="Arrow left" width="100%" height="100%" lazyload" />',
                '<img src="../../../../assets/img/icon-slide-right.png" alt="Arrow right" width="100%" height="100%" lazyload" />',
            ],
            responsive: {
                0: {
                    items: 1,

                },
                768: {
                    items: 2
                },
                1025: {
                    items: 3,
                }
            },
        }
    }

    public partner = {
        list: [],
        send: () => {
            this.globals.send({
                path: this.token.partner,
                token: "getPartner"
            })
        },
        options: {
            autoplay: true,
            autoplaySpeed: 3500,
            loop: true,
            nav: true,
            dots: false,
            navText: [
                '<img src="../../../../../assets/img/left-arrow.png" width="30" height="100%" alt="Previous">',
                '<img src="../../../../../assets/img/right-arrow.png" width="30" height="100%" alt="Next">'
            ],
            responsive: {
                0: {
                    items: 2,
                    touchDrag: true,
                },
                1025: {
                    items: 4,
                    dots: false,
                    mouseDrag: false,
                }
            }
        }
    }
}
