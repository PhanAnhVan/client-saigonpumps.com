import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Subscription } from 'rxjs';
import { Globals } from '../../globals';
import { TableService } from '../../services/integrated/table.service';
import { ToslugService } from '../../services/integrated/toslug.service';

@Component({
    selector: 'app-document',
    templateUrl: './document.component.html',
    styleUrls: ['./document.component.css'],
    providers: [ToslugService]
})
export class DocumentComponent implements OnInit {
    public connect: Subscription;
    public flag: boolean = true;
    public data: any = {};
    public show: number = -1;
    public document = new TableService();

    public token: any = {
        getDocument: "api/getListDocument",
        getSearchDocument: "api/getSearchDocument"
    }
    constructor(
        public globals: Globals,
        public route: ActivatedRoute,
        public router: Router,
        public toSlug: ToslugService,
        private title: Title,
    ) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case "getListDocument":
                    this.data = response.data;
                    this.search.group = this.search.compaid(this.data.group);
                    this.search.type = this.data.type;
                    this.search.field = this.data.field;
                    this.search.agencies = this.data.agencies;
                    this.document._concat(this.data.list, true);
                    this.title.setTitle(this.data.name);
                    break;

                case 'getNotification':
                    this.notification.data = response.data;
                    break;

                case 'getSearchDocument':
                    this.flag = true;
                    this.data = {};
                    this.data = response.data;
                    this.show = this.data.list && this.data.list.length > 0 ? 1 : 0;
                    this.document._concat(this.data.list, true);
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.document._ini({
            data: [],
            keyword: 'getListDocument',
            count: this.Pagination.itemsPerPage,
            sorting: {
                field: "maker_date",
                sort: "DESC",
                type: "date"
            }
        });
        this.globals.send({
            path: this.token.getDocument,
            token: "getListDocument"
        });
        this.notification.send();
    }

    public Pagination = {
        maxSize: 5,
        itemsPerPage: 20,
        change: (event: PageChangedEvent) => {
            const startItem = (event.page - 1) * event.itemsPerPage;
            const endItem = event.page * event.itemsPerPage;
            this.document.data = this.document.cached.slice(startItem, endItem);
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    }

    public notification = {
        token: 'api/getNotification',
        data: <any>{},
        send: () => {
            this.globals.send({ path: this.notification.token, token: "getNotification", params: { limit: 5 } });
        },
    }

    search = {
        group_id: 0,
        field_id: 0,
        type_id: 0,
        agencies_id: 0,
        value: '',
        group: [],
        field: [],
        type: [],
        agencies: [],
        cols: [
            { title: 'FEDocument.stt', field: 'index', show: true },
            { title: 'FEDocument.document', field: 'document', show: true },
            { title: 'FEDocument.download', field: 'download', show: true },
            { title: 'FEDocument.view', field: 'view', show: true },
        ],
        compareDate: (value) => {
            let date = new Date().valueOf();
            value = new Date(value).valueOf();
            if (date > value) {
                value = 'Hết hiệu lực'
            } else {
                value = 'Còn hiệu lực'
            }
            return value;
        },
        onSearch: () => {
            if (this.flag) {
                this.flag = false;
                if ((this.search.value.trim() != '' && !this.search.checkCharacterSpecial(this.search.value.trim())) || this.search.value == '' || this.search.value.trim() == '') {
                    this.globals.send({
                        path: this.token.getSearchDocument, token: 'getSearchDocument',
                        params: {
                            idGroup: +this.search.group_id,
                            idField: +this.search.field_id,
                            idType: +this.search.type_id,
                            idAgencies: +this.search.agencies_id,
                            value: this.toSlug._ini(this.search.value) || '',
                        }
                    });
                }
                this.search.value = '';
            }
        },
        checkCharacterSpecial: (str) => {
            let format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
            return format.test(str);
        },
        compaid(data) {
            let list = [];
            data = data.filter(function (item) {
                let v = (isNaN(+item.parent_id) && item.parent_id) ? 0 : +item.parent_id;
                v == 0 ? '' : list.push(item);
                return v == 0 ? true : false;
            })
            let compaidmenu = (data, skip, level = 0) => {
                level = level + 1;
                if (skip == true) {
                    return data;
                } else {
                    for (let i = 0; i < data.length; i++) {
                        let obj = data[i]['data'] && data[i]['data'].length > 0 ? data[i]['data'] : []
                        list = list.filter(item => {
                            let skip = (+item.parent_id == +data[i]['id']) ? false : true;
                            if (skip == false) { obj.push(item); }
                            return skip;
                        })
                        let skip = (obj.length == 0) ? true : false;
                        data[i]['data'] = compaidmenu(obj, skip, level);
                    }
                    return data;
                }
            };
            return compaidmenu(data, false);
        },

    }
}