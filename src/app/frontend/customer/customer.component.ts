import { Component, OnInit, OnDestroy } from "@angular/core";
import { Globals } from "../../globals";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";

@Component({
    selector: 'app-customer',
    templateUrl: './customer.component.html',
    styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

    public connect;
    public width: number = document.body.getBoundingClientRect().width;

    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,

    ) {

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case "getCustomer":
                    this.customer.data = response.data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.customer.send();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    public customer = {
        token: "api/getCustomer",
        data: <any>{},
        send: () => {
            this.globals.send({ path: this.customer.token, token: "getCustomer" });
        },
    };

}
