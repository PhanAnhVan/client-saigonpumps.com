import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'box-about-service',
  templateUrl: './box-about-service.component.html',
  styleUrls: ['./box-about-service.component.scss']
})
export class BoxAboutServiceComponent implements OnInit {

  @Input('item') item;

  constructor() { }

  ngOnInit() { }

}
