import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Globals } from 'src/app/globals';

@Component({
	selector: 'app-menu-horizontal',
	templateUrl: './menu-horizontal.component.html',
	styleUrls: ['./menu-horizontal.component.scss']
})
export class MenuHorizontalComponent implements OnInit {
	private connect: Subscription;
	@Input('menu') menu: any;

	public width: number = window.innerWidth;
	public link: string;

	constructor(
		public globals: Globals,
		public router: Router,
		public route: ActivatedRoute,
	) {
		this.router.events.subscribe(() => this.link = window.location.pathname.slice(1));

		this.connect = this.globals.result.subscribe((res: any) => {
			switch (res.token) {

				default:
					break;
			}
		});
	}

	ngOnInit() {

	}


	ngOnDestroy() { this.connect.unsubscribe() }
}