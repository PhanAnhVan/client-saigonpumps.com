import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-box-service',
	templateUrl: './box-service.component.html',
	styleUrls: ['./box-service.component.scss']
})
export class BoxServiceComponent implements OnInit {
	@Input('item') item: any;

	public width = document.body.getBoundingClientRect().width;

	constructor() { }

	ngOnInit() { }
}
