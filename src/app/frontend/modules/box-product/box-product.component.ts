import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-box-product',
    templateUrl: './box-product.component.html',
    styleUrls: ['./box-product.component.scss']
})
export class BoxProductComponent implements OnInit {
    @Input('item') item: any;
    @Input('type') type: any;

    @Input('title') titleBoxHomePage: number;

    public width = document.body.getBoundingClientRect().width;

    constructor() { }

    ngOnInit() { }

    formatPrice(number) {
        number = number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
        return number;
    }
}
