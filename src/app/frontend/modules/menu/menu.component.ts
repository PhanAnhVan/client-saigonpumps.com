import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Globals } from 'src/app/globals';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit, OnDestroy {
    @Input('menu') menu: any;
    @Output("menuStatus") menuStatus = new EventEmitter();

    private connect: Subscription;
    public width: number = window.innerWidth;
    public itemActive: number = 1;
    public statusOnMobile: boolean = true;

    constructor(
        public globals: Globals,
        public router: Router,
        public route: ActivatedRoute,
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                default:
                    break;
            }
        });
    }

    ngOnInit() { }

    ngOnDestroy() { this.connect.unsubscribe(); }

    _handleCloseOnMobile = () => this.menuStatus.emit(!1);

    showItem = item => { this.itemActive = (this.itemActive != item.id ? item.id : -1) }

    routerLink = item => { this.router.navigate(["/" + item.href]), this.menuStatus.emit(!1) };

    routerLinkUser = link => { this.menuStatus.emit(!1), this.router.navigate(["/" + link]) };

    infoUser = () => { this.menuStatus.emit(!1), this.router.navigate(['/user/thong-tin-ca-nhan']) }

    logout() {
        this.globals.CUSTOMER.remove(true);
        this.router.navigate(['/']);
    }
}
