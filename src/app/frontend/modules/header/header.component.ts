import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { Globals } from 'src/app/globals';
import { CartService } from 'src/app/services/apicart/cart.service';
import { ToslugService } from 'src/app/services/integrated/toslug.service';
import { ModalSinginSingupComponent } from '../../modal-singin-singup/modal-singin-singup.component';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    providers: [ToslugService, CartService],
})
export class HeaderComponent implements OnInit, OnDestroy {
    public menu: any;
    public connect: Subscription;
    public categories: any;
    public menuStatus: boolean = false;
    public modalRef: BsModalRef;
    @ViewChild('inputSearch') inputSearch: ElementRef;

    public token = {
        productCategory: "api/productCategory",
        menu: "api/getmenu",
    }
    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,
        public toSlug: ToslugService,
        public routerAtc: ActivatedRoute,
        public cartApi: CartService,
        public modalService: BsModalService
    ) {
        if (window['scrollmenu']) {
            window['scrollmenu']()
        }
        this.router.events.subscribe(e => {
            if (e instanceof NavigationEnd) {
                this.search.icon = false;
            }
        })
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "productCategory":
                    this.categories = res.data
                    break;

                case "getMenuMain":
                    this.menu = this.compaid(res.data)
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({
            path: this.token.menu,
            token: 'getMenuMain',
            params: { position: 'menuMain' }
        });
    }

    ngOnDestroy() { this.connect.unsubscribe(); }

    changeMenuStatus = (e: boolean) => {
        this.menuStatus = e;
        let b = document.querySelector("body"), h = document.querySelector("html");

        this.menuStatus ? (b.classList.add('overflow-hidden'), h.classList.add('scrollbar-hidden')) : (b.classList.remove('overflow-hidden'), h.classList.remove('scrollbar-hidden'));
    }

    logout() {
        this.globals.CUSTOMER.remove(true);
        this.router.navigate(['/']);
    }

    login = (type: any) => {
        this.modalRef = this.modalService.show(ModalSinginSingupComponent, {
            class: 'gray modal-md',
            initialState: { data: type }
        });
    }

    public search = {
        value: '',
        icon: <boolean>false,
        params: <any>{},
        onSearch: () => {
            this.search.value = this.toSlug._ini(this.search.value.replace(/\s+/g, " ").trim());
            if (this.search.check_search(this.search.value)) {
                this.search.params.value = this.search.value;
                this.router.navigate(['/tim-kiem'], {
                    queryParams: this.search.params,
                    queryParamsHandling: 'merge'
                });
            }
            this.search.value = '';
            this.search.icon = !this.search.icon;
            this.inputSearch.nativeElement.blur();
        },
        check_search: (value: string) => {
            let skip = true;
            skip = (value.toString().length > 0 && value != '' && value != '-' && value != '[' && value != ']' && value != '\\' && value != '{' && value != '}') ? true : false;
            return skip;
        },
        showSearch: () => {
            this.search.icon = !this.search.icon;
            this.inputSearch.nativeElement.focus();
        }
    }

    compaid(data: any[]) {
        let list = [];
        data = data.filter(function (item: { parent_id: string | number; }) {
            let v = (isNaN(+item.parent_id) && item.parent_id) ? 0 : +item.parent_id;
            v == 0 ? '' : list.push(item);
            return v == 0 ? true : false;
        })
        let compaidmenu = (data: string | any[], skip: boolean, level = 0) => {
            level = level + 1;
            if (skip == true) {
                return data;
            } else {
                for (let i = 0; i < data.length; i++) {
                    let obj = data[i]['data'] && data[i]['data'].length > 0 ? data[i]['data'] : []
                    list = list.filter(item => {
                        let skip = (+item.parent_id == +data[i]['id']) ? false : true;
                        if (skip == false) { obj.push(item); }
                        return skip;
                    })
                    let skip = (obj.length == 0) ? true : false;
                    data[i]['level'] = level;
                    data[i]['href'] = data[i]['link'];
                    data[i]['data'] = level >= 2 ? [] : compaidmenu(obj, skip, level);
                }
                return data;
            }
        };
        return compaidmenu(data, false);
    }

    _handleCloseNotification = () => {
        const notification = document.getElementById('notification');

        notification.classList.remove('d-block');
    };
}
