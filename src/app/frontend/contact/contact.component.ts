import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Globals } from 'src/app/globals';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent implements OnInit, OnDestroy {
  public connect: Subscription;
  public fm: FormGroup;
  public company: any;
  public flags: boolean = true;
  public width: number;
  public type: number;
  public flagSubmit: boolean = !1;
  public token: any = {
    getpageslink: 'api/pages/detail'
  };

  constructor(
    public globals: Globals,
    public fb: FormBuilder,
    public toastr: ToastrService,
    public route: ActivatedRoute,
    public router: Router,
    private title: Title,
  ) {
    this.width = document.body.getBoundingClientRect().width;

    const navigation = this.router.getCurrentNavigation();
    this.type = navigation.extras.state ? +navigation.extras.state.type : 1;

    this.contact.fmConfigs(this.type);

    this.connect = this.globals.result.subscribe((res: any) => {
      switch (res.token) {
        case 'addContact':
          this.flags = true;
          let type = res['status'] == 1 ? 'success' : (res['status'] == 0 ? 'warning' : 'danger');
          this.toastr[type](res['message'], type, { timeOut: 1500 });
          res.status == 1 ? this.contact.fmConfigs() : false
          break;

        case 'pagesContact':
          this.contact.data = res.data;
          this.title.setTitle(this.contact.data.name);
          break;

        default:
          break;
      }
    });
  }

  ngOnInit() {
    this.route.params.subscribe(() => {
      const path = window.location.pathname.slice(1);
      let link = path ? path : 'lien-he';

      this.globals.send({
        path: this.token.getpageslink,
        token: 'pagesContact',
        params: { link: link }
      });
    });
  }

  ngOnDestroy() { this.connect.unsubscribe() }

  public contact = {
    data: <any>{},
    token: 'api/contact/add',
    fmConfigs: (item: any = '') => {
      item = typeof item === 'object' ? item : { type: this.type };
      this.fm = this.fb.group({
        name: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)]],
        phone: ['', [Validators.required, Validators.pattern(/^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d+$)$/)]],
        subject: ['', [Validators.required]],
        type: item.type ? item.type : 0,
        message: ['', [Validators.required]],
      });
    },
    addContact: () => {
      if (this.flags && this.fm.valid) {
        this.flags = false;
        let data = this.fm.value;
        this.globals.send({
          path: this.contact.token,
          token: 'addContact',
          data: data,
        })
      }
    }
  };
}
