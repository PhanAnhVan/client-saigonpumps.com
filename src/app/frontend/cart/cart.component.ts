import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { CartService } from '../../services/apicart/cart.service';
import { Globals } from '../../globals';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ModalSinginSingupComponent } from '../modal-singin-singup/modal-singin-singup.component';
@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss'],
    providers: [CartService]
})
export class CartComponent implements OnInit, OnDestroy {
    @Output("event") eventOutput = new EventEmitter();
    fm: FormGroup;
    public modalRef: BsModalRef;
    public width: number = document.body.getBoundingClientRect().width;
    public datacart = [];
    public flags = true;
    private connect;
    public step: number = 1;
    public user = this.globals.CUSTOMER.get();
    public alert = { skip: false, message: '' };
    public cartComplete: any = { skip: false, code: '' };

    public token: any = {
        addcart: "api/customer/addCart",
        paymentCart: "api/cart/payment",
    }
    constructor(
        public cart: CartService,
        public fb: FormBuilder,
        public router: Router,
        public routerAct: ActivatedRoute,
        public globals: Globals,
        private toastr: ToastrService,
        public modalService: BsModalService,
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case "addcart":
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
                    this.order.loading = false;
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    if (res.status == 1) {
                        this.cart.clear();
                        this.eventOutput.emit(true);
                        Object.keys(res.data.customer).length > 0 ? this.globals.CUSTOMER.set(res.data.customer) : '';
                        this.cartComplete.code = res.data.code;
                        this.cartComplete.skip = true
                    }
                    break;

                case "paymentCart":
                    this.payment.data = res.data
                    break;

                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.fmConfigs(this.user);
        this.globals.send({ path: this.token.paymentCart, token: "paymentCart" });
        this.order.getlist();
    }

    fmConfigs(item: any = "") {
        item = typeof item === 'object' ? item : {};
        this.fm = this.fb.group({
            id: [item.id],
            name: [item.name],
            email: [item.email],
            password: [item.password],
            phone: [item.phone ? item.phone : '', [Validators.required, Validators.pattern(/^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d+$)$/)]],
            address: [item.address ? item.address : '', [Validators.required]],
            note: ''
        })
    }

    ngOnDestroy() { this.connect.unsubscribe() }

    public order = {

        data: [],

        loading: false,

        getlist: () => {
            this.order.data = this.cart.get();
        },

        changeAmount: (amount, index) => {
            if (amount <= 0 || !Number.isInteger(amount)) {
                this.order.data[index].amount = 1;
            }
        },

        removeItem: (id) => {
            this.cart.remove(id);
            this.order.getlist();
        },

        addCart: (amount, id) => {
            let data = this.cart.reduce();
            if (data[id]) {
                data[id].amount = +amount;
            }
            this.cart.edit(data[id], id);
        },

        submit: () => {

            let data = { cart: this.order.getDataCart(), cartdetail: this.order.getItemDetail(), item: this.order.getItem() };

            if (this.flags) {
                this.flags = !this.flags;
                this.order.loading = true;
                data.cart['customer_id'] = this.globals.CUSTOMER.get().id;
                this.globals.send({ path: this.token.addcart, token: "addcart", data: data });
            }
        },

        getItem: () => {
            let res = {
                id: this.user.id ? this.user.id : this.fm.value.id,
                email: this.user.email ? this.user.email : this.fm.value.email,
                password: this.user.password ? this.user.password : this.fm.value.password,
                name: this.user.name ? this.user.name : this.fm.value.name,
                phone: this.fm.value.phone ? this.fm.value.phone : '',
                address: this.fm.value.address ? this.fm.value.address : '',
                data: this.cart.get(),
            }
            return res;
        },

        getDataCart: () => {
            let cart = {
                code: (new Date()).valueOf(),
                day_start: new Date(),
                delivery_status: 1,
                customer_id: this.user.id,
                delivery_address: this.datacart['address'],
                price_total: this.cart.total(),
                phone: this.datacart['phone'],
                amount_total: this.cart.amount(),
                note: this.datacart['note'],
                status: 1,
            }
            this.order.getItem();
            return cart;
        },

        getItemDetail: () => {
            return Object.values(this.cart.get().reduce((n, o, i) => {
                n[i] = {
                    product_id: +o.id_products,

                    amount: o.amount,

                    price: +o.price,

                    images: o.images,

                    attribute: JSON.stringify(o.attribute),

                    total: (isNaN(+o.amount) ? 0 : +o.amount) * (isNaN(+o.price) ? 0 : +o.price),

                    status: 1,

                    note: (o.note) ? o.note : ''
                };
                return n;
            }, {}));
        },

        dataCart: () => {
            this.datacart = this.fm.value;
            if (!this.fm.valid) {
                this.alert.skip = true;
                this.alert.message = 'Bạn chưa nhập đầy đủ thông tin giao hàng.Vui lòng kiểm tra lại.';
                window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
                return false;
            } else {
                this.alert.skip = false;
                this.alert.message = '';
                window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
                return true;
            }
        },
    }

    login = (type: number) => {
        this.modalRef = this.modalService.show(ModalSinginSingupComponent, { class: 'gray modal-md', initialState: { data: type } });
        this.modalRef.content.onClose.subscribe((result) => {
            if (result == true) {
                setTimeout(() => {
                    this.fmConfigs(this.globals.CUSTOMER.get());
                    let status = this.order.dataCart();
                    this.step = status == false ? 2 : 3;
                    this.payment.skip = true;
                }, 200);
            }
        });
    }

    public payment = {
        data: [],
        skip: false,
        check: (skip) => {
            if (!this.globals.CUSTOMER.check()) {
                skip == false ? this.router.navigate(['/dang-nhap'], { queryParams: { checkcart: true } }) : this.login(1);
            } else {
                let status = this.order.dataCart();
                this.step = status == false ? 2 : 3;
                this.payment.skip = true;
            }
        },
        onStep: (step) => {
            this.datacart = this.fm.value;
            this.payment.skip = true;
            this.step = step;
        }
    }
}
