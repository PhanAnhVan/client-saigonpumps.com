import { Component, OnInit, OnDestroy, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../../globals';
import { TableService } from '../../services/integrated/table.service';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/public_api';
import { ToslugService } from '../../services/integrated/toslug.service';
import { Meta, Title } from '@angular/platform-browser';

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.scss'],
    providers: [ToslugService]

})
export class PageComponent implements OnInit, OnDestroy {

    public detail: any;
    public pageView: number = -1;
    public data = <any>[];
    public connect;
    public cwstable = new TableService();
    public token: any = {
        getpageslink: 'api/pages/detail',
    }
    constructor(
        public route: ActivatedRoute,
        public globals: Globals,
        public router: Router,
        public toSlug: ToslugService,
        private title: Title,
        private meta: Meta,
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "pagesdetail":
                    if (res.data && Object.values(res.data).length > 0) {
                        this.data = res.data;
                        this.title.setTitle(this.data.name);
                        this.meta.updateTag({ name: 'description', content: this.data.description });
                        if (this.data.service && this.data.service.length > 0) {
                            this.data.service = this.pagination.ini(this.data.service);
                        }
                        this.pageView = 1;

                    } else {
                        this.pageView = 0
                    }
                    break;

                default:
                    break;
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let link = params.link;
            let parent_link = params.parent_link || '';
            this.globals.send({ path: this.token.getpageslink, token: "pagesdetail", params: { link: link, parent_link: parent_link } });
        })
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    renderHtml = () => {
        let main = document.getElementById("contentDetail");
        if (main) {
            let el = main.querySelectorAll("table");
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement("div");
                    div.className = "table-responsive table-bordered m-0 border-0";
                    el[i].parentNode.insertBefore(div, el[i]);
                    el[i].className = 'table';
                    el[i].setAttribute('class', 'table');
                    let cls = el[i].getAttribute('class');
                    el[i]
                    let newhtml = "<table class='table'>" + el[i].innerHTML + "</table>";
                    el[i].remove();
                    div.innerHTML = newhtml;
                }
            }
            let image = main.querySelectorAll("img");
            if (image) {
                for (let i = 0; i < image.length; i++) {
                    let a = document.createElement("div");
                    a.className = "images-detail d-inline";
                    image[i].parentNode.insertBefore(a, image[i]);
                    let src = image[i].currentSrc;
                    let html = `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` + src + `">
                        `+ image[i].outerHTML + `
                    </a>`
                    image[i].remove()
                    a.innerHTML = html;
                }
            }
        }
    }

    public pagination = {
        total: 0,
        maxSize: 5,
        itemsPerPage: 12,
        data: [],
        ini: (data) => {
            this.pagination.total = data.length;
            this.pagination.data = data;
            return data.slice(0, this.pagination.itemsPerPage);
        },
        changed: (event: PageChangedEvent) => {
            const start = (event.page - 1) * event.itemsPerPage;
            const end = event.page * event.itemsPerPage;
            this.data.service = this.pagination.data.slice(start, end);
            window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
        },
    };
}
