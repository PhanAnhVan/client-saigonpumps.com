import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Globals } from 'src/app/globals';

@Component({
    selector: 'app-content-list',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.scss'],
})
export class ListContentComponent implements OnInit, OnChanges, OnDestroy {
    public connect;
    public show: number = -1;
    public data: any = {};
    @Input('link') link: string = ''
    public token: any = {
        getContent: "api/content/list",
        getContentGroup: 'api/content/group',
        getContentListGroup: 'api/content/listGroup'
    }
    constructor(
        public globals: Globals,
        public route: ActivatedRoute,
        public router: Router
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getContent":
                    this.data = res.data;
                    this.Pagination.ini(res.data.list);
                    this.show = this.data.list && this.data.list.length > 0 ? 1 : 0;
                    setTimeout(() => this.group.handleGetListByGroup(this.data.group[0].id));
                    break;

                case 'getContentListGroup':
                    this.group.list = res.data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() { }

    ngOnChanges() {
        if (this.link && this.link != '') {
            this.globals.send({
                path: this.token.getContent,
                token: "getContent",
                params: { link: this.link }
            })
        }
    }

    ngOnDestroy() { this.connect.unsubscribe(); }

    public group = {
        active: 0,
        list: [],
        handleGetListByGroup: (id: number) => {
            this.globals.send({
                path: this.token.getContentListGroup,
                token: 'getContentListGroup',
                params: {
                    limit: 6,
                    id: id
                }
            })
        }
    }

    public Pagination = {
        total: 0,
        maxSize: 5,
        itemsPerPage: 10,
        data: [],
        ini: (data) => {
            this.Pagination.total = data.length;
            this.Pagination.data = data;
            this.data.list = data.slice(0, this.Pagination.itemsPerPage);
        },
        changed: (event: PageChangedEvent) => {
            const start = (event.page - 1) * event.itemsPerPage;
            const end = event.page * event.itemsPerPage;
            this.data.list = this.Pagination.data.slice(start, end);
            window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
        },
    };
}