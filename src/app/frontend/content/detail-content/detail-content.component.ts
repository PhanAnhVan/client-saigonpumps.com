import { Component, OnDestroy, OnInit, } from '@angular/core';
import { Globals } from '../../../globals';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToslugService } from '../../../services/integrated/toslug.service';
import { Subscription } from 'rxjs';
import { Meta, Title } from '@angular/platform-browser';

@Component({
    selector: 'app-detail-content',
    templateUrl: './detail-content.component.html',
    styleUrls: ['./detail-content.component.scss'],
    providers: [ToslugService]

})
export class DetailContentComponent implements OnInit, OnDestroy {
    public connect: Subscription;
    public data = <any>{};
    public category = [];
    public parent_link: any;
    public link = "";
    public width: number;
    public titleBoxDetailContent = 0;
    public token: any = {
        getContentDetail: "api/content/detail",
    }
    constructor(
        public globals: Globals,
        public fb: FormBuilder,
        public route: ActivatedRoute,
        public router: Router,
        public toSlug: ToslugService,
        private title: Title,
        private meta: Meta,
    ) {
        this.width = document.body.getBoundingClientRect().width;
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getContentDetail":
                    this.data = res.data;

                    if (res.data.keywords && res.data.keywords.length > 0) {
                        this.data.keywords = JSON.parse(res.data.keywords);
                    }
                    this.title.setTitle(this.data.name);
                    this.meta.updateTag({ name: 'description', content: this.data.description });
                    this.meta.updateTag({ name: 'keywords', content: this.data.keywords });
                    setTimeout(() => {
                        this.renderHtml();
                    }, 500);
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.link = params.link;
            setTimeout(() => {
                this.globals.send({ path: this.token.getContentDetail, token: "getContentDetail", params: { link: this.link } });
            }, 50);
        });

        this.globals.send({ path: this.token.getContentCategory, token: "getContentCategory" });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    renderHtml = () => {
        let main = document.getElementById("contentDetail");
        if (main) {
            let el = main.querySelectorAll("table");
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement("div");
                    div.className = "table-responsive table-bordered m-0 border-0";
                    el[i].parentNode.insertBefore(div, el[i]);
                    el[i].className = 'table';
                    el[i].setAttribute('class', 'table');
                    let cls = el[i].getAttribute('class');
                    el[i]
                    let newhtml = "<table class='table'>" + el[i].innerHTML + "</table>";
                    el[i].remove();
                    div.innerHTML = newhtml;
                }
            }
            let image = main.querySelectorAll("img");
            if (image) {
                for (let i = 0; i < image.length; i++) {
                    let a = document.createElement("div");
                    a.className = "images-detail d-inline";
                    image[i].parentNode.insertBefore(a, image[i]);
                    let src = image[i].currentSrc;
                    let html = `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` + src + `">
                        `+ image[i].outerHTML + `
                    </a>`
                    image[i].remove()
                    a.innerHTML = html;
                }
            }
        }
    }

    public search = {

        value: '',

        params: <any>{},

        onSearch: (value: string) => {
            this.search.value = this.toSlug._ini(value.replace(/\s+/g, " ").trim());
            if (this.search.check_search(this.search.value)) {
                this.search.params.value = this.search.value;
                this.router.navigate(['/tim-kiem'], {
                    queryParams: { value: this.search.value, type: 'news' }, queryParamsHandling: 'merge'
                });

            }
            this.search.value = '';
        },

        check_search: (value: string) => {
            let skip = true;
            skip = (value.toString().length > 0 && value != '' && value != '-' && value != '[' && value != ']' && value != '\\' && value != '{' && value != '}') ? true : false;
            return skip;
        }
    }


}