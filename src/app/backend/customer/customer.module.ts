import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GetlistComponent } from "./getlist/getlist.component";
import { ProcessComponent } from "./process/process.component";
import { ChangespasswordComponent } from "./changespassword/changespassword.component";

const routes: Routes = [
    { path: "", redirectTo: "get-list" },
    { path: "get-list", component: GetlistComponent },
    { path: "insert", component: ProcessComponent },
    { path: "update/:id", component: ProcessComponent },
    { path: 'changespassword/:id', component: ChangespasswordComponent },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        TranslateModule,
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        GetlistComponent,
        ProcessComponent,
        ChangespasswordComponent
    ],
})
export class CustomerModule { }
