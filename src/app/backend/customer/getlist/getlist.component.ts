import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals'
import { AlertComponent } from '../../modules/alert/alert.component';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-getlist-customer',
    templateUrl: './getlist.component.html',
})

export class GetlistComponent implements OnInit, OnDestroy {

    modalRef: BsModalRef;

    public connect;

    public token: any = {
        getlist: "get/customer/getlist",
        remove: "set/customer/remove"
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true, filter: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblEmail', field: 'email', show: true, filter: true },
        { title: 'lblPhone', field: 'phone', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true },
        { title: '', field: 'status', show: true }
    ];

    public cstable = new TableService();

    constructor(

        public globals: Globals,

        private modalService: BsModalService,

        public toastr: ToastrService,
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "getlistcustomer":
                    this.cstable.sorting = { field: "maker_date", sort: "DESC", type: "", };
                    this.cstable._concat(res.data, true);
                    break;

                case "removecustomer":
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getlist, token: 'getlistcustomer' });
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {

        this.globals.send({ path: this.token.getlist, token: 'getlistcustomer' });

        this.cstable._ini({ cols: this.cols, data: [], keyword: 'customer', count: 50 });
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe();
        }
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    onRemove(id: number, name: any) {
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'customer.remove', name: name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removecustomer', params: { id: id || 0 } });
            }
        });
    }
}
