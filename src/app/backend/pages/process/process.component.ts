import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { Globals } from "../../../globals";
import { TagsService } from "../../../services/integrated/tags.service";
import { uploadFileService } from "../../../services/integrated/upload.service";
import { LinkService } from "../../../services/integrated/link.service";

@Component({
    selector: "app-process",
    templateUrl: "./process.component.html",
})

export class ProcessComponent implements OnInit, OnDestroy {

    fm: FormGroup;

    public connect: any;

    public token = {
        process: "set/pages/process",
        getRow: "get/pages/getrow",
        getlistPage: "get/pages/getlist",
    };

    public id: number;

    public listPages: any = [];

    public images = new uploadFileService();

    public listimages = new uploadFileService();

    public type: number = 2;

    public isOpenDev = false;

    public modalRef: BsModalRef;

    public optionFields = [
        { name: "Video", empty: "", field: "link_video", enable: false },
        { name: "products.listimages", empty: "[]", field: "listimages", enable: false }
    ]

    constructor(
        public fb: FormBuilder,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute,
        public tags: TagsService,
        public link: LinkService,
        private modalService: BsModalService
    ) {
        this.routerAct.params.subscribe((params) => {
            this.id = +params["id"];
            if (this.id && this.id != 0) {
                this.getRow();
            } else {
                this.fmConfigs();
            }
        });

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res["token"]) {

                case "getRow":
                    let data = res["data"];
                    this.fmConfigs(data);
                    this.getlistPage();
                    this.configOption(data)
                    break;

                case "PagesProcess":
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type, { timeOut: 1500 });
                    if (res["status"] == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + "/pages/get-list"]);
                        }, 500);
                    }
                    break;

                case "getpagesPages":
                    this.listPages = res.data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.getlistPage();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    getRow() {
        this.globals.send({ path: this.token.getRow, token: "getRow", params: { id: this.id } });
    }

    getlistPage() {
        this.globals.send({ path: this.token.getlistPage, token: "getpagesPages", params: { type: this.type }, });
    }

    fmConfigs(item: any = "") {
        item =
            typeof item === "object" ? item : { status: 1, parent_id: 0, orders: 0 };
        this.fm = this.fb.group({
            name: [item.name ? item.name : "", [Validators.required]],
            orders: +item.orders ? +item.orders : 0,
            link: item.link ? item.link : "",
            parent_id: +item.parent_id ? +item.parent_id : 0,
            detail: item.detail ? item.detail : "",
            description: item.description ? item.description : "",
            icon: item.icon ? item.icon : "",
            link_video: item.link_video ? item.link_video : "",
            title: item.title ? item.title : "",
            keywords: item.keywords ? item.keywords : "",
            status: item.status && item.status == 1 ? true : false,
        });

        const imagesConfig = {
            path: this.globals.BASE_API_URL + "public/pages/",
            data: item.images ? item.images : "",
        };
        const listimagesConfig = {
            path: this.globals.BASE_API_URL + "public/pages/",
            data: item.listimages ? item.listimages : "",
            multiple: true,
        };
        this.images._ini(imagesConfig);
        this.listimages._ini(listimagesConfig);
        let keywords = item.keywords ? JSON.parse(item.keywords) : [];
        this.tags._set(keywords);
    }

    onChangeLink(e) {
        const url = this.link._convent(e.target.value);
        this.fm.value.link = url;
    }

    onSubmit() {
        if (this.fm.valid) {
            let data: any = this.fm.value;
            data.status = data.status == true ? 1 : 0;
            data.keywords = this.tags._get();
            data.link = this.link._convent(data.link);
            data.images = this.images._get(true);
            data.listimages = this.listimages._get(true);
            data.type = 2;
            this.globals.send({ path: this.token.process, token: "PagesProcess", data: data, params: { id: this.id || 0 } });
        }
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    public option(field) {
        let option;
        this.optionFields.forEach(f => {
            if (f.field === field) { option = f }
        })
        return option
    }

    configOption(data) {
        this.optionFields.forEach(f => {
            if (data[f.field] && data[f.field].toString().length > 0 && data[f.field].toString() != f.empty) {
                f.enable = true
            }
        })
    }

    optionChanged(option) {
        option.enable = !option.enable;
    }
}
