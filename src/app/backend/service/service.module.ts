import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { CKEditorModule } from 'ckeditor4-angular';

import { GetListComponent } from './get-list/get-list.component';
import { ProcessComponent } from './process/process.component';
import { ProductComponent } from './product/product.component';

export const routes: Routes = [
    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: GetListComponent },
    { path: 'insert', component: ProcessComponent },
    { path: 'update/:id', component: ProcessComponent },
    { path: 'product/:id', component: ProductComponent },

];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        CKEditorModule
    ],
    declarations: [
        GetListComponent,
        ProcessComponent,
        ProductComponent
    ]
})
export class ServiceModule { }
