import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup } from '@angular/forms';
import { Globals } from '../../../globals';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, OnDestroy {

    fm: FormGroup;

    public connect: any;

    public token: any = {
        getListProduct: 'get/pages/getlistproducts',
        getProductsService: 'get/pages/getproductsservice',
        getListGroupProduct: "get/pages/grouptype",
        processProductService: 'set/pages/processProductService'
    }

    public id: number;

    public serviceName: string = '';

    public cols = [
        { title: '', field: 'name', filter: true },
    ]

    public product = {
        data: [],
        group: [],
        valueSearch: '',
        activeChoose: 0,
        _concat: (data) => {
            let list = JSON.parse(data || '[]').reduce((n, o) => { n[+o] = o; return n }, []);
            for (let i = 0; i < this.product.data.length; i++) {
                let item = this.product.data[i];
                item.checked = list[item.id] ? true : false;
            }
        },
        _count: () => {
            let count = 0;
            for (let i = 0; i < this.product.data.length; i++) {
                let item = this.product.data[i];
                count += (item.checked ? 1 : 0);
            }
            return count;
        },
        _filter: (id, checked = false) => {
            this.product.activeChoose = (checked ? 1 : 0);
            let data = [];
            if (!checked) {
                data = (id == 0) ? this.product.data : this.product.data.filter((res: any) => res.page_id == id);
            } else {
                data = this.product.data.filter((res: any) => res.checked == true);
            }
            this.cwstable.data = [];
            this.cwstable._concat(data, true);
        },
        _checkedStatusAll: () => {
            for (let i = 0; i < this.cwstable.data.length; i++) {
                let item = this.cwstable.data[i];
                if (!item.checked) {
                    return false;
                }
            }
            return true;
        },
        _checkAll: (checked) => {
            for (let i = 0; i < this.cwstable.data.length; i++) {
                let item = this.cwstable.data[i];
                item.checked = checked;
            }
        },
        _get: () => {
            let result = (this.product.data.filter((res: any) => res.checked == true)).reduce((n, o) => { n[o.id] = o; return n }, []);
            return Object.keys(result);
        },
        _submit: () => {
            let data = { related: JSON.stringify(this.product._get()) };
            this.globals.send({ path: this.token.processProductService, token: 'processProductService', data: data, params: { id: this.id } })
        }
    }

    public cwstable = new TableService();

    constructor(
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case 'getListProduct':
                    this.product.data = res.data;
                    this.cwstable.data = [];
                    this.cwstable._concat(res.data, true);
                    this.globals.send({ path: this.token.getProductsService, token: 'getProductsService', params: { id: this.id } })
                    break;

                case 'getListGroupProduct':
                    this.product.group = res.data;
                    break;

                case 'getProductsService':
                    this.serviceName = res.data.name;
                    this.product._concat(res.data.related);
                    break;

                case 'processProductService':
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getProductsService, token: 'getProductsService', params: { id: this.id } })
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit(): void {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'getProductsServices', count: 100 });
        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
            if (this.id && this.id != 0) {
                this.globals.send({ path: this.token.getListProduct, token: 'getListProduct' });
                this.globals.send({ path: this.token.getListGroupProduct, token: 'getListGroupProduct', params: { type: 3 } });

            } else {
                this.router.navigate(['/admin/service/getlist'])
            }
        })

    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }
}