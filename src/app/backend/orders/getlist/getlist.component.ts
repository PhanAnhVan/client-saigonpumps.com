import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals';
import { AlertComponent } from '../../modules/alert/alert.component';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
    styleUrls: ['./getlist.component.css']
})

export class GetlistComponent implements OnInit, OnDestroy {

    public connect: any;

    public token: any = {
        getlist: "get/order/getlist",
        remove: "set/order/remove",
        getUnRead: "get/dashboard/getUnRead",
    }

    private cols = [
        { title: "lblStt", field: "index", show: true },
        { title: "order.code", field: "code", show: true },
        { title: "order.customer", field: "customer_name", show: true },
        { title: "order.delivery_address", field: "delivery_address", show: true },
        { title: "order.amount_total", field: "amount_total", show: true },
        { title: "order.price_total", field: "price_total", show: true },
        { title: "status.title", field: "status", show: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: "lblAction", field: "action", show: true }
    ];

    public cwstable = new TableService();

    private modalRef: BsModalRef;

    constructor(
        public globals: Globals,
        public toastr: ToastrService,
        public modalService: BsModalService
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case 'getlist':
                    this.cwstable.sorting = { field: "maker_date", sort: "DESC", type: "", };
                    this.cwstable._concat(res.data, true);
                    break;

                case 'remove':
                    this.showNotification(res);
                    this.globals.send({ path: this.token.getUnRead, token: 'getUnRead' });
                    if (res.status == 1) {
                        this.getList();
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'order', sorting: { field: "id", sort: "DESC", type: "number" } });
        this.getList();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    formatPrice(number: { toString: () => string; }) {
        number = number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
        return number;
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getlist' });
    }

    onRemove(id: number, code: any) {
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'order.remove', name: code } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'remove', params: { id: id } });
            }
        });
    }
}
