import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { Globals } from '../../../globals';
import { ToslugService } from '../../../services/integrated/toslug.service';
import { AlertComponent } from '../../modules/alert/alert.component';
import { AddCustomerComponent } from '../add-customer/add-customer.component';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css'],
    providers: [ToslugService]
})

export class ProcessComponent implements OnInit, OnDestroy {

    private connect;

    modalRef: BsModalRef;

    public fmProduct: FormGroup;

    public fmInfo: FormGroup;

    public id: number = 0;

    public token: any = {

        process: "set/order/process",

        getrow: "get/order/getrow",

        getListServices: "get/order/getListServices",

        getListProducts: "get/pages/getlistproducts",

        getListDocuments: "get/productconfig/document/getlist",

        getListCustomers: "get/order/getListCustomers",

        getUnRead: "get/dashboard/getUnRead",

        getDocumentProduct: 'get/order/getDocumentProduct',

        processDocumentProduct: 'set/order/processDocumentProduct'
    }

    @ViewChild('inputSearchProduct') private inputSearchProduct: ElementRef;

    public product = {
        name: '',
        item: <any>false,
        data: [],
        valueSearch: '',
        field: ['name', 'group_name'],
        open: <boolean>true,
        _select: (item) => {
            this.globals.send({ path: this.token.getDocumentProduct, token: 'getDocumentProduct', params: { id: item.id } });
            this.product.name = item.name;
            this.product.item = item;
            this.fmProduct.controls['product_id'].setValue(item.id);
            this.fmProduct.controls['price'].setValue(Number(item.price_sale > 0 ? item.price_sale : item.price).toLocaleString('vi'));
        },
        _add: () => {
            let data = this.fmProduct.value;
            data.amount = data.amount.toString().replace(/\./g, '');
            data.price = data.price.toString().replace(/\./g, '');

            let document_customer = this.document._get();
            let item = {
                id: data.product_id,
                name: this.product.name,
                amount: +data.amount || 1,
                price: +data.price,
                total: +data.amount * +data.price,
                service_id: data.service_id,
                service_name: this.service.name,
                document_customer: document_customer,
                count_document: document_customer.length,
                guarantee: data.guarantee.trim(),
            }
            this.listProduct.data.push(item);
            this.listProduct._sumTotal();
            this.product._reset();
            this.service._reset();
            this.document._reset();
        },
        _clear: () => {
            this.product.valueSearch = '';
            this.product.item = false;
        },
        _focusSearch: () => {
            setTimeout(() => {
                this.inputSearchProduct.nativeElement.focus();
            }, 300);
        },
        _reset: () => {
            this.product._clear();
            this.fmProduct.controls['product_id'].setValue(0);
            this.fmProduct.controls['price'].setValue(0);
            this.fmProduct.controls['guarantee'].setValue('');
            this.fmProduct.controls['amount'].setValue(1);
            this.product.name = '';
        }
    }

    public service = {
        name: '',
        data: [],
        _select: (item) => {
            this.service.name = (item && item.name ? item.name : '');
            this.fmProduct.controls['service_id'].setValue((item && item.id ? item.id : 0));
        },
        _reset: () => {
            this.service.name = '';
            this.fmProduct.controls['service_id'].setValue(0);
        }
    }

    public document = {
        data: [],
        valueSearch: '',
        field: ['name'],
        _get: () => {
            let result = [];
            for (let i = 0; i < this.document.data.length; i++) {
                let item = this.document.data[i];
                if (item.checked) { result.push({ document_customer_id: 0, id: item.id, name: item.name, checked: true }); }
            }
            return result;
        },
        _reset: () => {
            for (let i = 0; i < this.document.data.length; i++) {
                let item = this.document.data[i];
                item.checked = false;
            }
        },
        _concat: (data) => {
            for (let i = 0; i < this.document.data.length; i++) {
                let item = this.document.data[i];
                item.checked = data[item.id] ? true : false;
            }
        },
        _save: (data, orderDetailId) => {
            let list = { add: [], remove: [] }
            data.filter((res: any) => {
                if (res.checked == true) {
                    list.add.push(res);
                } else if (res.checked == false && res.document_customer_id > 0) {
                    list.remove.push(res);
                }
            });

            this.globals.send({ path: this.token.processDocumentProduct, token: 'processDocumentProduct', data: list, params: { order_detail_id: orderDetailId } });
        }
    }

    public listProduct = {
        cols: [
            { title: "lblStt", field: "index" },
            { title: "order.product_name", field: "name" },
            // { title: "order.service", field: "service_name" },
            { title: "order.guarantee", field: "guarantee" },
            { title: "order.amount", field: "amount" },
            { title: "order.price", field: "price" },
            { title: "order.total", field: "total" },
            { title: "lblAction", field: "action" }
        ],
        data: [],
        remove: [],
        _concat: (data) => {
            this.listProduct.data = data;
        },
        _sumTotal: () => {
            let total = 0;
            for (let i = 0; i < this.listProduct.data.length; i++) {
                let item = this.listProduct.data[i];
                total += +item.total;
            }
            this.fmInfo.controls['amount_total'].setValue(this.listProduct.data.length);
            this.fmInfo.controls['price_total'].setValue(total);
        },
        _get: () => {
            let result = [];
            for (let i = 0; i < this.listProduct.data.length; i++) {
                let item = this.listProduct.data[i];
                let obj = {
                    id: item.order_detail_id || 0,
                    product_id: item.id,
                    service_id: item.service_id,
                    customer_id: this.fmInfo.value.customer_id,
                    amount: item.amount,
                    price: item.price,
                    total: item.total,
                    guarantee: item.guarantee,
                    status: 1,
                    document_customer: item.document_customer = item.document_customer.filter((res: any) => res.checked == true)
                }
                result.push(obj);
            }
            return { add: result, remove: this.listProduct.remove };
        },
        _remove: (item, index) => {
            this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'products.remove', name: item.name } });
            this.modalRef.content.onClose.subscribe(result => {
                if (result == true) {
                    if (item.order_detail_id && item.order_detail_id > 0) {
                        let document_customer = (item.document_customer || []).reduce((n, o) => { n[o.document_customer_id] = o; return n }, []);
                        this.listProduct.remove.push({ order_detail_id: item.order_detail_id, document_customer: Object.keys(document_customer) });
                    }
                    this.listProduct.data = this.listProduct.data.filter((res, i) => i != index);
                    this.listProduct._sumTotal();
                }
            });
        }
    }

    @ViewChild('inputSearchCustomer') private inputSearchCustomer: ElementRef;
    public customer = {
        data: [],
        item: <any>false,
        valueSearch: '',
        field: ['name', 'email', 'address', 'phone'],
        _select: (item) => {
            this.customer.item = item;
            this.fmInfo.controls['customer_id'].setValue(+item.id);
        },
        _concat: (id) => {
            let item = this.customer.data.filter((res: any) => res.id == id);
            if (item.length > 0) {
                this.customer._select(item[0]);
            }
        },
        _clear: () => {
            this.customer.valueSearch = '';
        },
        _add: () => {
            this.modalRef = this.modalService.show(AddCustomerComponent, { initialState: {}, class: 'modal-lg' });
            this.modalRef.content.onClose.subscribe((result) => {
                if (result) {
                    this.customer._select(result);
                    this.customer.data.unshift(result);
                }
            });
        },
        _focusSearch: () => {
            setTimeout(() => {
                this.inputSearchCustomer.nativeElement.focus();
            }, 300);
        },
    }

    public status = {
        title: '',
        value: 0,
        data: [
            { title: 'order.cancelled', value: 0 },
            { title: 'order.processing', value: 1 },
            { title: 'order.processed', value: 2 }
        ],
        _select: (item) => {
            this.status.title = item.title;
            this.fmInfo.controls['delivery_status'].setValue(item.value);
        },
        _concat: (value) => {
            this.status.value = +value;
            this.status.title = this.status.data.filter((res: any) => res.value == value)[0].title;
        }
    }

    public flag: boolean = true;

    public loading: boolean = true;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        public globals: Globals,
        private toastr: ToastrService,
        private routerAct: ActivatedRoute,
        public modalService: BsModalService
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "getrow":
                    let data = res.data;

                    this.fmConfigsInfo(data.info);
                    this.customer._concat(data.info.customer_id);

                    data.list = data.list.filter(item => item.attribute = (item.attribute != null && item.attribute != '' && (item.attribute).toString().length > 4) ? JSON.parse(item.attribute) : []);

                    this.listProduct._concat(data.list);
                    this.loading = false;
                    break;

                case 'getListServices':
                    this.service.data = res.data;
                    break;

                case 'getListProducts':
                    this.product.data = res.data;


                    this.globals.send({ path: this.token.getListCustomers, token: 'getListCustomers' });
                    break;

                case 'getListDocuments':
                    this.document.data = res.data;
                    break;

                case 'getListCustomers':
                    this.customer.data = res.data;
                    if (this.id > 0) {
                        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
                    } else {
                        this.loading = false;
                    }
                    break;

                case 'getDocumentProduct':
                    let list = res.data.reduce((n, o, i) => {
                        n[o.document_id] = o;
                        return n;
                    }, []);
                    this.document._concat(list);
                    break;

                case "process":
                    this.showNotification(res);
                    this.flag = true;
                    this.globals.send({ path: this.token.getUnRead, token: 'getUnRead' });
                    if (res.status == 1) {
                        if (this.id == 0) {
                            setTimeout(() => {
                                this.router.navigate(['admin/orders/update/' + res.data]);
                            }, 100);
                        } else {
                            this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
                        }
                    }
                    break;

                case "processDocumentProduct":
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
                    }
                    break;

                default:
                    break;
            }
        });
    }


    ngOnInit() {
        this.globals.send({ path: this.token.getListProducts, token: 'getListProducts' });
        this.globals.send({ path: this.token.getListServices, token: 'getListServices' });
        this.globals.send({ path: this.token.getListDocuments, token: 'getListDocuments' });

        this.routerAct.params.subscribe(params => {
            this.id = +params['id'] || 0;
            if (this.id == 0) {
                this.fmConfigsInfo();
            }
        });

        this.fmConfigsProduct();
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    formatPrice(number: { toString: () => string; }) {
        number = number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
        return number;
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : (res.status == 0 ? "warning" : "danger");
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    fmConfigsProduct(data: any = "") {

        data = typeof data === 'object' ? data : {};

        this.fmProduct = this.fb.group({

            product_id: [data.product_id ? data.product_id : null, [Validators.required]],

            service_id: data.service_id ? data.service_id : 0,

            amount: [data.amount ? data.amount : 1, Validators.min(1)],

            price: [data.price ? data.price : 0, [Validators.min(1)]],

            guarantee: data.guarantee ? data.guarantee : ''
        })
    }

    fmConfigsInfo(data: any = "") {

        data = typeof data === 'object' ? data : { status: 1 };

        this.fmInfo = this.fb.group({

            code: data.code ? data.code : '',

            customer_id: [data.customer_id ? data.customer_id : null, [Validators.required]],

            phone: [data.phone ? data.phone : '', Validators.pattern("^[0-9]*$")],

            delivery_address: data.delivery_address ? data.delivery_address : '',

            amount_total: [data.amount_total ? data.amount_total : 0, Validators.min(1)],

            price_total: [data.price_total ? data.price_total : 0],

            note: data.note ? data.note : '',

            status: data.status ? data.status : 0,

            delivery_status: data.delivery_status ? data.delivery_status : 0

        })

        this.status._concat(+data.delivery_status || 0);
    }

    onSubmit() {

        if (this.flag) {

            this.flag = false;

            let data = { info: this.fmInfo.value, list: this.listProduct._get() };

            this.globals.send({ path: this.token.process, token: "process", data: data, params: { id: this.id } });
        }
    }
}
