import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CKEditorModule } from 'ckeditor4-angular';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { GetlistComponent } from './getlist/getlist.component';
import { ProcessContentComponent } from './process/process.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: GetlistComponent },
    { path: 'insert', component: ProcessContentComponent },
    { path: 'update/:id', component: ProcessContentComponent },
]
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(appRoutes),
        ModalModule.forRoot(),
        TranslateModule,
        CKEditorModule,
        TooltipModule.forRoot(),
        BsDatepickerModule.forRoot(),
    ],
    declarations: [
        GetlistComponent,
        ProcessContentComponent,
    ],
})
export class DocumentModule { }
