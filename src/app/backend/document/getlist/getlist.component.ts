import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewContainerRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';
import { AlertComponent } from 'src/app/backend/modules/alert/alert.component';
import { Globals } from 'src/app/globals';
import { TableService } from 'src/app/services/integrated/table.service';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
})
export class GetlistComponent implements OnInit, OnChanges, OnDestroy {
    private id: number;

    public show;

    private name: any;

    modalRef: BsModalRef;

    @Input("filter") filter: any;

    @Input("change") change: any;

    public connect;

    public token: any = {

        getlist: "get/document/getlist",
        remove: "set/document/remove",
        changePin: "set/document/changepin",
    }

    private cols = [

        { title: 'Mã', field: 'code', show: true, filter: true },

        { title: 'documents.name', field: 'name', show: true, filter: true },

        { title: 'documents.startDate', field: 'start_date', show: true, filter: true },

        // { title: 'documents.endDate', field: 'end_date', show: true, filter: true },

        { title: 'lblAction', field: 'action', show: true },

        { title: '', field: 'status', show: true },
    ];

    public cstable = new TableService();

    constructor(

        public globals: Globals,

        private modalService: BsModalService,

        public toastr: ToastrService,


        vcr: ViewContainerRef
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "getListDocument":
                    this.cstable.sorting = { field: "maker_date", sort: "DESC", type: "" };
                    this.cstable._concat(res.data, true);
                    break;

                case "removecontent":
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    if (res.status == 1) {
                        this.cstable._delRowData(this.id);
                    }
                    break;

                case "changepin":
                    type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getlist, token: "getListDocument" });
                    }
                    break;

                default:
                    break;
            }
        });


    }

    ngOnInit() {
        this.globals.send({ path: this.token.getlist, token: 'getListDocument' });

        this.cstable._ini({ cols: this.cols, data: [], keyword: 'content', count: 50 });
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    ngOnChanges(e: SimpleChanges) {

        if (typeof this.filter === 'object') {

            let value = Object.keys(this.filter);

            if (value.length == 0) {

                this.cstable._delFilter('page_id');

            } else {

                this.cstable._setFilter('page_id', value, 'in', 'number');
            }
        }
    }
    onRemove(id: number, name: any) {
        this.id = id;

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'documents.remove', name: name } });

        this.modalRef.content.onClose.subscribe(result => {

            if (result == true) {

                this.globals.send({ path: this.token.remove, token: 'removecontent', params: { id: id } });
            }
        });
    }

    pinContent = (id, pin, id_group) => {
        this.globals.send({ path: this.token.changePin, token: 'changepin', params: { id: id, pin: pin == 1 ? 0 : 1, idGroup: id_group } });
    }
}
