import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Globals } from 'src/app/globals';
import { uploadFileService } from 'src/app/services/integrated/upload.service';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
})
export class ProcessContentComponent implements OnInit, OnDestroy {

    public connect;
    fm: FormGroup;
    public id: number = 0;
    public flag: boolean = true;

    public file = new uploadFileService();
    uploadFileService
    public token: any = {
        getrow: "get/document/getrow",
        process: "set/document/process",
    }
    constructor(

        public fb: FormBuilder,
        public globals: Globals,
        public toastr: ToastrService,
        public router: Router,
        public routerAct: ActivatedRoute,

    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
        });

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getrow":
                    this.fmConfigs(res.data)
                    break;

                case "processDocument":
                    this.flag = true;
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    if (res.status == 1) {
                        setTimeout(() => {
                            this.router.navigate(['admin/document/get-list']);
                        }, 1000);
                    }
                    break;

                case "getGroupDoc":
                    this.document.groupDocument = res.data;
                    break;

                case "getAgenciesDoc":
                    this.document.agencies = res.data;
                    break;

                case "getFieldDoc":
                    this.document.field = res.data;
                    break;

                case "getTypeDoc":
                    this.document.typeDocument = res.data;
                    break;

                default:
                    break;
            }
        });
    }
    ngOnInit() {
        this.document.send();
        if (this.id && this.id != 0) {
            this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
        } else {
            this.fmConfigs();
        }

    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    fmConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { status: 1, start_date: new Date(), end_date: new Date() };

        this.fm = this.fb.group({

            code: [item.code ? item.code : '', [Validators.required]],

            name: [item.name ? item.name : '', [Validators.required]],

            field_id: [item.field_id ? item.field_id : ''],

            agencies_id: [item.agencies_id ? item.agencies_id : ''],

            group_document_id: [item.group_document_id ? item.group_document_id : ''],

            type_document_id: [item.type_document_id ? item.type_document_id : ''],

            start_date: [item.start_date ? new Date(item.start_date) : new Date()],

            end_date: [item.end_date ? new Date(item.end_date) : new Date()],

            status: (item.status && item.status == 1) ? true : false,

        });

        this.file._ini({ path: this.globals.BASE_API_URL + 'public/document/', data: item.file ? item.file : '', type: 2 });
    }



    onSubmit() {
        if (this.fm.valid && this.flag) {
            this.flag = false;
            const obj = this.fm.value;

            obj.status === true ? obj.status = 1 : obj.status = 0;

            obj.file = this.file._get(true);

            this.globals.send({ path: this.token.process, token: 'processDocument', data: obj, params: { id: this.id || 0 } });

        }
    }

    document = {
        token: {
            groupDocument: 'get/groupDocument/getlist',
            agencies: "get/agencies/getlist",
            field: "get/field/getlist",
            typeDocument: "get/typeDocument/getlist",
        },

        groupDocument: [],
        agencies: [],
        field: [],
        typeDocument: [],

        send: () => {
            this.globals.send({ path: this.document.token.groupDocument, token: 'getGroupDoc' });
            this.globals.send({ path: this.document.token.agencies, token: 'getAgenciesDoc' });
            this.globals.send({ path: this.document.token.field, token: 'getFieldDoc' });
            this.globals.send({ path: this.document.token.typeDocument, token: 'getTypeDoc' });
        }
    }
}
