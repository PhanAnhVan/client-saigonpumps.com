import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { TableService } from '../../../services/integrated/table.service';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-groupgetlist',
    templateUrl: './groupgetlist.component.html',
    styleUrls: ['./groupgetlist.component.css']
})

export class GroupgetlistComponent implements OnInit, OnDestroy {

    @Output("filter") filter = new EventEmitter();

    public connect;

    public token: any = {
        getlist: "get/pages/grouptype",
    }

    private cols = [
        { title: 'lblGroup', field: 'name', show: true, filter: true },
        { title: 'lblCount', field: 'count_content', show: true, filter: true },
    ];

    public table = new TableService();

    constructor(
        public globals: Globals
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "contentgroup":
                    this.table._concat(res.data, true);
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getlist, token: 'contentgroup', params: { type: 4 } });
        this.table._ini({ cols: this.cols, data: [], keyword: 'contentgroup', count: 5 });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    onCheckItem(item) {
        item.check = (item.check == true) ? false : true;
        this.filter.emit(item.id);
    }
}
