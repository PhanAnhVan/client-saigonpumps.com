import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals';
import { uploadFileService } from '../../../services/integrated/upload.service';
import { TagsService } from '../../../services/integrated/tags.service';
import { LinkService } from '../../../services/integrated/link.service';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
})

export class ProcessContentComponent implements OnInit, OnDestroy {

    public connect: any;

    fm: FormGroup;

    public id: number = 0;

    public group: any = [];

    public images = new uploadFileService();

    public file = new uploadFileService();

    public token: any = {
        getrow: "get/content/getrow",
        process: "set/content/process",
        getgroupcontent: "get/pages/grouptype",
    }

    constructor(

        public fb: FormBuilder,

        public globals: Globals,

        public link: LinkService,

        public toastr: ToastrService,

        public router: Router,

        public routerAct: ActivatedRoute,

        public tags: TagsService
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
        });

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "getrow":
                    let data = res.data;
                    this.fmConfigs(data)
                    break;

                case "getgroupcontent":
                    this.group = res.data;
                    break;

                case "processContent":
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    if (res.status == 1) {
                        setTimeout(() => {
                            this.router.navigate(['admin/contents/get-list']);
                        }, 1000);
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {

        this.globals.send({ path: this.token.getgroupcontent, token: 'getgroupcontent', params: { type: 4 } });

        if (this.id && this.id != 0) {
            this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
        } else {
            this.fmConfigs()
        }

    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    fmConfigs(item: any = "") {
        item = typeof item === 'object' ? item : { status: 1, maker_date: new Date() };
        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            orders: +item.orders ? +item.orders : '',
            link: [item.link ? item.link : '', [Validators.required]],
            detail: [item.detail ? item.detail : '',],
            page_id: [item.page_id ? item.page_id : '', [Validators.required]],
            description: [item.description ? item.description : '',],
            note: item.note ? item.note : '',
            author: item.author ? item.author : '',
            pin: item.pin ? item.pin : '',
            maker_date: item.maker_date ? new Date(item.maker_date) : '',
            status: (item.status && item.status == 1) ? true : false,
        });
        const imagesConfig = { path: this.globals.BASE_API_URL + 'public/contents/', data: item.images ? item.images : '' };
        this.images._ini(imagesConfig);
        let keywords = item.keywords ? JSON.parse(item.keywords) : [];
        this.tags._set(keywords);
        this.file._ini({ path: this.globals.BASE_API_URL + 'public/file/', data: item.file ? item.file : '', type: 2 });
    }

    onChangeLink(e) {
        const url = this.link._convent(e.target.value);
        this.fm.value.link = url;
    }

    onSubmit() {
        if (this.fm.valid) {
            const obj = this.fm.value;
            obj.images = this.images._get(true);
            obj.keywords = this.tags._get();
            obj.link = this.link._convent(obj.link);
            obj.status === true ? obj.status = 1 : obj.status = 0;
            obj.file = this.file._get(true);
            obj.maker_date = this.globals.time.format(obj.maker_date);
            this.globals.send({ path: this.token.process, token: 'processContent', data: obj, params: { id: this.id || 0 } });
        }
    }
}
