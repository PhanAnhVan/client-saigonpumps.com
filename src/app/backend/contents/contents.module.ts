import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CKEditorModule } from 'ckeditor4-angular';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { GetlistComponent } from './getlist/getlist.component';
import { GroupgetlistComponent } from './groupgetlist/groupgetlist.component';
import { MainComponent } from './main/main.component';
import { ProcessContentComponent } from './process/process.component';

const appRoutes: Routes = [
	{ path: '', redirectTo: 'get-list' },
	{ path: 'get-list', component: MainComponent },
	{ path: 'insert', component: ProcessContentComponent },
	{ path: 'update/:id', component: ProcessContentComponent }
]
@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(appRoutes),
		BsDatepickerModule.forRoot(),
		TranslateModule,
		CKEditorModule
	],
	declarations: [
		GetlistComponent,
		GroupgetlistComponent,
		ProcessContentComponent,
		MainComponent
	]
})
export class ContentsModule { }
