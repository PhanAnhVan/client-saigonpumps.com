import { Component, OnInit, ViewContainerRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../../globals';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
})
export class ProcessComponent implements OnInit, OnDestroy {

    public id: number = 0;

    public fm: FormGroup;

    private connect;

    private token: any = {

        process: "set/field/process",
        getrow: "get/field/getrow",
    }

    constructor(
        public fb: FormBuilder,
        public router: Router,
        private toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute,
        public translate: TranslateService,
    ) {


        this.routerAct.params.subscribe(params => {
            this.id = +params.id
        })
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'getrow':
                    let data = response['data'];
                    this.fmConfigs(data);
                    break;
                case 'processField':
                    let type = (response['status'] == 1) ? "success" : (response['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](response['message'], type, { timeOut: 1500 });
                    if (response['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/setting/field/get-list']);
                        }, 2000);
                    }
                    break;



                default:
                    break;
            }
        });
    }
    ngOnInit() {
        if (this.id && this.id != 0) {
            this.getRow();
        } else {
            this.fmConfigs();
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getRow() {
        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
    }

    fmConfigs(item: any = "") {
        item = typeof item === 'object' ? item : { status: 1, type: 1 };
        this.fm = this.fb.group({
            id: item.id ? item.id : '',
            name: [item.name ? item.name : '', [Validators.required]],
            code: item.code ? item.code : '',
            status: (item.status && item.status == 1) ? true : false,
        });
    }

    onChangeCode(e) {
        this.fm.controls['code'].setValue(e.target.value);

    }

    onSubmit() {
        if (this.fm.valid) {
            var data: any = this.fm.value;
            data.status = data.status == true ? 1 : 0;

            this.globals.send({ path: this.token.process, token: 'processField', data: data, params: { id: this.id || 0 } })
        }
    }
}
