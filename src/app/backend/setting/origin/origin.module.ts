import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { OriginlistComponent } from './get-list/get-list.component';
import { OriginProcessComponent } from './process/process.component';

export const routes: Routes = [
    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: OriginlistComponent },
    { path: 'insert', component: OriginProcessComponent },
    { path: 'update/:id', component: OriginProcessComponent },
];

@NgModule({

    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
    ],
    declarations: [
        OriginlistComponent,
        OriginProcessComponent
    ],
})
export class OriginModule { }
