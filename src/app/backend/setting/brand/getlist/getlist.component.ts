import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Globals } from '../../../../globals';
import { TableService } from '../../../../services/integrated/table.service';
import { AlertComponent } from '../../../modules/alert/alert.component';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
})

export class GetlistComponent implements OnInit, OnDestroy {

    public connect: any;

    public token: any = {
        getlist: "get/brand/getlist",
        remove: "set/brand/remove"
    }

    modalRef: BsModalRef;

    public id: any;

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true },
        { title: '', field: 'status', show: true, filter: true },
    ];

    constructor(
        public globals: Globals,
        public table: TableService,
        private modalService: BsModalService,
        public toastr: ToastrService
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res['token']) {

                case 'getList':
                    this.table._concat(res['data'], true);
                    break;

                case 'removeBrand':
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type, { timeOut: 1500 });
                    if (res['status'] == 1) {
                        this.table._delRowData(this.id)
                    }
                    break;

                default:
                    break;
            }
        });
    }


    ngOnInit() {
        this.getList();
        this.table._ini({ cols: this.cols, data: [], keyword: 'brand' });
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getList' });
    }

    onRemove(item: any): void {
        this.id = item.id;
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'brand.remove', name: item.name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removeBrand', params: { id: item.id, name: item.images } });
            }
        });
    }
}
