import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../../globals';
import { TableService } from '../../../../services/integrated/table.service';
import { AlertComponent } from '../../../modules/alert/alert.component';

@Component({
    selector: 'app-get-list-type',
    templateUrl: './get-list-type.component.html',
})

export class GetListTypeComponent implements OnInit, OnDestroy {

    public connect: any;

    public type: number = 0;

    public translateTitle: string = ''

    modalRef: BsModalRef;

    public id: number;

    public token: any = {
        getlist: "get/pages/getlist",
        remove: "set/pages/remove",
        changestatus: "set/pages/changestatus",
        changePin: 'set/pages/changePin'
    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblparent_name', field: 'parent_name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true },
    ];

    public cwstable = new TableService();

    constructor(
        private modalService: BsModalService,
        public routerAtc: ActivatedRoute,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals
    ) {

        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'getListPages', count: 50 });

        this.translateTitle = this.router.url.split('/')[3];

        switch (this.router.url.split('/')[3]) {
            case 'link':
                this.type = 1
                break;
            case 'product':
                this.type = 3
                break;
            case 'content':
                this.type = 4
                break;
            case 'library':
                this.type = 5
                break;
            case 'customer':
                this.type = 6
                break;
            default:
                break;
        }
        this.getList(this.type);

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case 'getListPages':
                    this.cwstable.data = []
                    this.cwstable.sorting = { field: "maker_date", sort: "DESC", type: "" };
                    this.cwstable._concat(res.data, true);
                    break;

                case 'removePages':
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.cwstable._delRowData(this.id)
                    }
                    break;

                case 'changePin':
                case 'changestatus':
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.getList(this.type)
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {

    }

    getList(type) {
        this.globals.send({ path: this.token.getlist, token: 'getListPages', params: { type: type } });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    onRemove(item: any): void {
        this.id = item.id;
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'pages.removePages', name: item.name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removePages', params: { id: item.id } });
            }
        });
    }

    changeStatus = (id, status) => {
        this.globals.send({ path: this.token.changestatus, token: 'changestatus', params: { id: id, status: status == 1 ? 0 : 1 } });
    }

    changePin = (id, pin) => {
        this.globals.send({ path: this.token.changePin, token: 'changePin', params: { id: id, pin: pin == 1 ? 0 : 1 } });
    }
}
