import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../../globals';
import { TagsService } from '../../../../services/integrated/tags.service';
import { uploadFileService } from '../../../../services/integrated/upload.service';
import { LinkService } from '../../../../services/integrated/link.service';

@Component({
    selector: 'app-process-type',
    templateUrl: './process-type.component.html',
})
export class ProcessTypeComponent implements OnInit, OnDestroy {

    fm: FormGroup;
    public connect: any;
    public token: any = {
        process: "set/pages/process",
        pathGetRowPages: "get/pages/getrow",
        pathGetPages: "get/pages/getlist"
    }
    public id: number;
    public required: boolean = false
    public listPages: any = [];
    public images = new uploadFileService();
    public type: number = 0
    constructor(
        public fb: FormBuilder,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute,
        public translate: TranslateService,
        public tags: TagsService,
        public link: LinkService
    ) {
        switch (this.router.url.split('/')[3]) {
            case 'link':
                this.type = 1
                break;
            case 'product':
                this.type = 3
                break;
            case 'content':
                this.type = 4
                break;
            case 'library':
                this.type = 5
                break;
            case 'customer':
                this.type = 6
                break;
            default:
                break;
        }
        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
            if (this.id && this.id != 0) {
                this.getRow();
            } else {
                this.fmConfigs();
                this.getListPages();
            }
        })

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'getPagesGroup':
                    this.listPages = response.data;
                    break;

                case 'GetRowPages':
                    let data = response['data'];
                    this.fmConfigs(data);
                    this.getListPages();
                    break;

                case 'PagesProcess':
                    let type = (response.status == 1) ? "success" : (response.status == 0 ? "warning" : "danger");
                    this.toastr[type](response.message, type, { closeButton: true });
                    if (response.status == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/setting/' + this.router.url.split('/')[3] + '/get-list']);
                        }, 100);
                    }
                    break;

                default:
                    break;
            }
        });
    }
    ngOnInit() {

    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    getListPages() {
        this.globals.send({ path: this.token.pathGetPages, token: 'getPagesGroup', params: { type: this.type } });
    }
    fmConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { status: 1, parent_id: 0, orders: 0 };

        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            orders: +item.orders ? +item.orders : 0,
            link: item.link ? item.link : '',
            parent_id: +item.parent_id ? +item.parent_id : 0,
            title: item.title ? item.title : '',
            detail: item.detail ? item.detail : '',
            icon: item.icon ? item.icon : '',
            description: item.description ? item.description : '',
            keywords: item.keywords ? item.keywords : '',
            status: (item.status && item.status == 1) ? true : false,
        });

        const imagesConfig = { path: this.globals.BASE_API_URL + 'public/pages/', data: item.images ? item.images : '' };

        this.images._ini(imagesConfig);

        this.tags._set(item.keywords ? JSON.parse(item['keywords']) : '');
    }
    onChangeLink(e) {
        const url = this.link._convent(e.target.value);

        this.fm.value.link = url;
    }
    getRow() {
        this.globals.send({ path: this.token.pathGetRowPages, token: 'GetRowPages', params: { id: this.id } });
    }
    onSubmit() {
        if (this.fm.valid) {

            let data: any = this.fm.value;

            data.status = (data.status == true) ? 1 : 0;

            data.keywords = this.tags._get();

            data.link = this.link._convent(data.link);

            data.images = this.images._get(true);

            data.type = this.type;

            this.globals.send({ path: this.token.process, token: 'PagesProcess', data: data, params: { id: this.id || 0 } });

        }
    }

}
