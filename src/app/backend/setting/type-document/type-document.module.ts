import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertModule } from 'ngx-bootstrap/alert';
import { GetlistComponent } from './getlist/getlist.component';
import { ProcessComponent } from './process/process.component';


const appRoutes: Routes = [
    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: GetlistComponent },
    { path: 'insert', component: ProcessComponent },
    { path: 'update/:id', component: ProcessComponent },
]

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(appRoutes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,

        AlertModule.forRoot()
    ],

    declarations: [
        GetlistComponent,
        ProcessComponent,
    ]
})
export class TypeDocumentModule { }
