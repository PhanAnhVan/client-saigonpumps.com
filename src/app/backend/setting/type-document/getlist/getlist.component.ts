import { Component, OnInit, OnDestroy } from '@angular/core';
import { TableService } from '../../../../services/integrated/table.service';
import { Globals } from '../../../../globals';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AlertComponent } from '../../../modules/alert/alert.component';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',

})
export class GetlistComponent implements OnInit, OnDestroy {
    public token: any = {
        getlist: "get/typeDocument/getlist",
        remove: "set/typeDocument/remove"
    }
    modalRef: BsModalRef;
    public connect: any;
    public id: any
    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true },
        { title: '#', field: 'status', show: true, filter: true },
    ];
    constructor(
        public globals: Globals,
        public table: TableService,
        private modalService: BsModalService,


        public toastr: ToastrService,


    ) {

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'getListTypeDocument':
                    this.table._concat(response['data'], true);
                    break;

                case 'removeTypeDocument':
                    let type = (response['status'] == 1) ? "success" : (response['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](response['message'], type, { timeOut: 1500 });
                    if (response['status'] == 1) {
                        this.table._delRowData(this.id)
                    }
                    break;

                default:
                    break;
            }
        });
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    ngOnInit() {
        this.getList();
        this.table._ini({ cols: this.cols, data: [], keyword: 'typeDocument', count: 50 });
    }
    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getListTypeDocument' });
    }
    onRemove(item: any): void {
        this.id = item.id;
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'typeDocument.remove', name: item.name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removeTypeDocument', params: { id: item.id, name: item.images } });
            }
        });
    }
}
