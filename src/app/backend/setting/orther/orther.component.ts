import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { Globals } from '../../../globals';
import { TagsService } from "../../../services/integrated/tags.service";
import { uploadFileService } from '../../../services/integrated/upload.service';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-orther',
    templateUrl: './orther.component.html'
})

export class OrtherComponent implements OnInit, OnDestroy {

    public connect: any;

    public data: any = [];

    modalRef: BsModalRef;

    public token: any = {
        process: "set/settings/setorther",
        getlist: "get/settings/getorther"
    }

    public item: any = { title: '', key: '', value: '', type: 'text', skip: false };

    private flag: boolean = true;

    public page = { group: 0, name: '' }

    constructor(
        private globals: Globals,
        private tags: TagsService,
        public table: TableService,
        public toastr: ToastrService,
        public images: uploadFileService,
        public modalService: BsModalService,
        public router: Router,
    ) {
        this.connect = this.globals.result.subscribe((response: any) => {

            switch (response['token']) {

                case 'otherGetlist':
                    this.data = [];
                    for (var key in response['data']) {
                        let item: any = response['data'][key];
                        item.skip == false;
                        if (item.type == 2) {
                            let configImages = { path: this.globals.BASE_API_URL + 'public/settings/', data: item.value, multiple: true };
                            item.images = new uploadFileService();
                            item.images._ini(configImages);
                        }
                        if (item.type == 3) {
                            this.tags._set(item.value);
                        }
                        this.data.push(item);
                    }
                    break;

                case 'settingsOtherProcess':
                    this.flag = true;
                    let type = (response['status'] == 1) ? "success" : (response['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](response['message'], type, { timeOut: 1500 });
                    if (response.status == 1) {
                        this.getlist();
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {

        switch (this.router.url.split('/')[3]) {
            case 'company':
                this.page.group = 2
                this.page.name = 'settings.company';
                break;
            case 'social-network':
                this.page.group = 1;
                this.page.name = 'settings.socialNetwork'
                break;
            case 'orther':
                this.page.group = 0;
                this.page.name = 'settings.other'
                break;
            default:
                break;
        }

        this.getlist();

    }

    getlist() {
        this.globals.send({ path: this.token.getlist, token: 'otherGetlist', params: { group: this.page.group || 0 } });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    onSubmit(item: any = {}) {

        if (this.flag && item.skip == true) {

            this.flag = false;

            let data: any = { text_key: item.text_key, value: item.value, title: item.title, type: item.type };

            if (data.type == 2) {
                data.value = item.images._get(true);
            }

            if (data.type == 3) {
                data.value = this.tags._get();
            }

            this.globals.send({ path: this.token.process, token: 'settingsOtherProcess', data: data, params: { id: item.id || 0 } });
        }

    }
}
