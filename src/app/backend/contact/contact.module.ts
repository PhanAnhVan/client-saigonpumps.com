import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { GetlistComponent } from './getlist/getlist.component';
import { InfoContactComponent } from './info-contact/info-contact.component';

const contactRoute: Routes = [
    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: GetlistComponent },
    { path: 'info-contact/:id', component: InfoContactComponent },
]
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(contactRoute),
        TranslateModule
    ],
    declarations: [
        GetlistComponent,
        InfoContactComponent,
    ]
})
export class ContactModule { }
