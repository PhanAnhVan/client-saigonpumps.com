import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Globals } from 'src/app/globals';
import { TableService } from 'src/app/services/integrated/table.service';
import { AlertComponent } from '../../modules/alert/alert.component';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html'
})
export class GetlistComponent implements OnInit, OnDestroy {

    public data: any;

    public id: number;

    public modalRef: BsModalRef;

    public connect;

    private cols = [

        { title: 'lblStt', field: 'index', show: true, filter: true },

        { title: 'contact.name', field: 'name', show: true, filter: true },

        { title: 'contact.email', field: 'email', show: true, filter: true },

        { title: 'contact.subject', field: 'subject', show: true, filter: true },

        { title: 'contact.type', field: 'type', show: true, filter: true },

        { title: 'contact.message', field: 'message', show: true, filter: true },

        { title: 'contact.maker_date', field: 'maker_date', show: true, filter: true },

        // { title: 'contact.id_checked', field: 'user_name', show: true, filter: true },

        { title: '#', field: 'action', show: true },

    ];

    public token: any = {
        getlist: "get/contact/getlist",
        remove: 'set/contact/remove'
    }

    constructor(
        private modalService: BsModalService,
        public toastr: ToastrService,
        public cwstable: TableService,
        public globals: Globals
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {

                case "getlist":
                    this.cwstable._concat(res.data, true);
                    break;

                case "removeContact":
                    let success = res.status == 1 ? true : false;

                    let status = success ? "success" : (res.status == 0 ? "warning" : "danger");

                    this.toastr[status](res.message, status, { closeButton: true });

                    success ? this.globals.send({ path: this.token.getlist, token: 'getlist' }) : false;

                    break;

                default:
                    break;
            }
        });

    }

    ngOnInit() {

        this.globals.send({ path: this.token.getlist, token: 'getlist' });

        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'contact', sorting: { field: "maker_date", sort: "DESC", type: "" } });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    onRemove(id: number, name: any) {

        this.id = id;

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'contact.mesRemoveContact', name: name } });

        this.modalRef.content.onClose.subscribe(result => {

            if (result == true) {

                this.globals.send({ path: this.token.remove, token: 'removeContact', params: { id: id || 0 } });
            }
        });
    }
}
