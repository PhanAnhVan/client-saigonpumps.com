import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals';
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-info-contact',
    templateUrl: './info-contact.component.html',
})

export class InfoContactComponent implements OnInit, OnDestroy {

    public data: any;

    public id: number;

    fm: FormGroup;

    public item: any;

    public connect;

    public infoContact: any = {};

    public name: any;

    public skip: any = false;

    public hidden: any = false;

    public token: any = {
        getrow: "get/contact/getrow",
        sendmail: "set/contact/sendmail",
        getlist: "get/contact/getlist",
        getUnRead: "get/dashboard/getUnRead",
    }

    constructor(

        public cwstable: TableService,

        public fb: FormBuilder,

        public toastr: ToastrService,

        public router: Router,

        public routerAct: ActivatedRoute,

        public globals: Globals
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
        })

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {
                case 'getrow':
                    this.infoContact = res['data'];
                    if (this.infoContact.subject_send || this.infoContact.message_send) {
                        this.hidden = true;
                    }
                    this.fm = this.fb.group({
                        id: [this.id],
                        name: [this.infoContact.name, [Validators.required]],
                        email: [this.infoContact.email, [Validators.required, Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)]],
                        phone: [this.infoContact.phone, [Validators.pattern(/[0-9\+\-\ ]/)]],
                        type: [`${this.infoContact.type == 0 ? 'Trở thành Đại lý' : 'Cần tư vấn lắp đặt'}`, [Validators.pattern(/[0-9\+\-\ ]/)]],
                        subject_send: ['', [Validators.required]],
                        message_send: ['', [Validators.required]],
                    })
                    break;

                case "sendmail":
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type, { timeOut: 1500 });
                    if (res['status'] == 1) {
                        this.globals.send({ path: this.token.getUnRead, token: 'getUnRead' });
                        this.getRow();
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.getRow()
    }

    getRow() {
        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    onSendMail() {
        this.skip = true
    }

    onSubmit() {

        let check = (this.fm.value) ? 1 : 0;

        let data = this.fm.value;

        data.checked = check;

        data.status = (this.fm.value) ? 1 : 0;

        this.globals.send({ path: this.token.sendmail, token: "sendmail", data: data, params: { id: this.id } });
    }
}
