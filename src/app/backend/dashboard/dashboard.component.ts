import { Component, OnInit, OnDestroy } from '@angular/core';
import { Globals } from '../../globals';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {

    public data = [];

    public connect: any;

    public token: any = {
        getDashboard: "get/dashboard/getlist",
    }

    constructor(
        public globals: Globals
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case 'getDashboard':
                    this.data = res.data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getDashboard, token: 'getDashboard' });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }
}
