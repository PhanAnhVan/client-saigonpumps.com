import { Component, OnInit, Input, OnDestroy, OnChanges } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

import { Globals } from '../../../globals';
import { AlertComponent } from '../../modules/alert/alert.component'
import { TableService } from '../../../services/integrated/table.service';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
})

export class GetlistComponent implements OnInit, OnDestroy, OnChanges {

    private id: number;

    public connect;

    public show: any;

    public token: any = {
        getlist: "get/product/getlist",
        remove: "set/product/remove",
        changeHot: "set/product/changeHot",
        getlistGroup: "get/product/grouptype",
        changeStatus: "set/product/changeStatus",
    }

    @Input("filter") filter: any;

    @Input("change") change: any;

    modalRef: BsModalRef;

    private cols = [
        { title: 'lblStt', field: 'index', show: false },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblImages', field: 'images', show: true },
        { title: 'products.nameGroup', field: 'namegroup', show: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true },
        { title: '#', field: 'status', show: true, filter: true },
    ];

    public table = new TableService();

    constructor(
        public globals: Globals,
        public toastr: ToastrService,
        public router: Router,
        private modalService: BsModalService,
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getlistproduct":
                    this.table.sorting = { field: "maker_date", sort: "DESC", type: "" };
                    this.table._concat(res.data, true);
                    break;

                case "removeproduct":
                    this.showNotification(res);
                    if (res['status'] == 1) {
                        this.table._delRowData(this.id);
                        this.globals.send({ path: this.token.getlistGroup, token: 'getlistproductgroup', params: { type: 3 } });
                    }
                    break;

                // case "changePin":
                case "changeHot":
                case 'changeStatus':
                    this.showNotification(res);
                    if (res.status == 1) {
                        this.getlist();
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.getlist();
        this.table._ini({ cols: this.cols, data: [], count: 50 });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    getlist = () => {
        this.globals.send({ path: this.token.getlist, token: 'getlistproduct' });
    }

    ngOnChanges() {

        if (typeof this.filter === 'object') {

            let value = Object.keys(this.filter);

            if (value.length == 0) {

                this.table._delFilter('page_id');

            } else {
                this.table._setFilter('page_id', value, 'in', 'number');
            }
        }
    }

    showNotification(res) {
        let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
        this.toastr[type](res.message, type, { timeOut: 1500 });
    }

    onRemove(item: any): void {

        this.id = item.id;

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'products.remove', name: item.name } });

        this.modalRef.content.onClose.subscribe(result => {

            if (result == true) {

                this.globals.send({ path: this.token.remove, token: 'removeproduct', params: { id: item.id } });
            }
        });
    }

    changeHot = (id, hot) => {
        this.globals.send({ path: this.token.changeHot, token: 'changeHot', params: { id: id, hot: hot == 1 ? 0 : 1 } });
    }

    changeStatus(id, status) {
        this.globals.send({ path: this.token.changeStatus, token: 'changeStatus', params: { id: id, status: status == 1 ? 0 : 1 } });
    }
}

