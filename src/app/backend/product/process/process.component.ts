import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { Globals } from '../../../globals';
import { LinkService } from '../../../services/integrated/link.service';
import { TagsService } from '../../../services/integrated/tags.service';
import { uploadFileService } from '../../../services/integrated/upload.service';
import { ToslugService } from '../../../services/integrated/toslug.service';
import { AlertComponent } from '../../modules/alert/alert.component';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css'],
    providers: [ToslugService]
})

export class ProcessProductComponent implements OnInit, OnDestroy {

    public id: number = 0;

    private connect;

    public fm: FormGroup;

    public price: FormGroup;

    public seo: FormGroup;

    modalRef: BsModalRef;

    public flags: boolean = true;

    private token: any = {

        getrow: "get/product/getrow",

        getlistproductgroup: "get/pages/grouptype",

        getlistOrigin: "get/origin/getlist",

        getlisttype: "get/type/getlist",

        getlistBrand: "get/brand/getlist",

        getlistAttribute: "get/attribute/getlistall",

        getlistDocument: "get/productconfig/document/getlist",

        getDocument: "get/product/getdocument",

        process: "set/product/process",

        updateSeo: "set/product/updateSeo",

        updatePrice: "set/product/updatePrice"

    }

    public data = { origin: [], brand: [], group: [], type: [] };

    public images = new uploadFileService();

    public listimages = new uploadFileService();

    @ViewChild('inputSearchGroup') private inputSearchGroup: ElementRef;

    public group = {
        name: '',
        item: <any>false,
        data: [],
        valueSearch: '',
        field: ['name', 'group_name'],
        open: <boolean>true,
        _select: (item) => {
            this.group.name = item.name;
            this.group.item = item;
            this.fm.controls['page_id'].setValue(item.id);
        },
        _clear: () => {
            this.group.valueSearch = '';
            this.group.item = false;
        },
        _focusSearch: () => {
            setTimeout(() => {
                this.inputSearchGroup.nativeElement.focus();
            }, 300);

        },
        _reset: () => {
            this.group._clear();
            this.fm.controls['page_id'].setValue(0);
            this.group.name = '';
        }
    }

    constructor(

        private routerAct: ActivatedRoute,

        public router: Router,

        public globals: Globals,

        private link: LinkService,

        private fb: FormBuilder,

        public tags: TagsService,

        private toastr: ToastrService,
        private modalService: BsModalService,

    ) {

        this.connect = this.globals.result.subscribe((res: any) => {
            let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
            switch (res.token) {

                case "getrow":
                    let data = res.data;
                    this.fmConfigs(data);
                    this.priceConfigs(data);
                    this.seoConfigs(data);
                    this.productImages.set(data);
                    if (data.attribute && data.attribute.length > 0) {
                        this.attribute._ini(data.attribute);
                    }
                    if (data.priceattr && data.priceattr.length > 0) {
                        let list = data.priceattr.filter((item: any) => item.att_id_value = JSON.parse(item.att_id_value));
                        setTimeout(() => {
                            this.priceAttribute.data = list.filter(item => {
                                const imagesConfig = { path: this.globals.BASE_API_URL + 'public/products/', data: item.images ? item.images : '' };
                                item.list = [], item.attr_id = [], item.images = new uploadFileService();
                                item.images._ini(imagesConfig)
                                if (item.att_id_value && item.att_id_value.length > 0) {
                                    item.att_id_value.forEach(element => {
                                        item.list.push(this.attribute.list[element]);
                                        let parent_id = this.attribute.list[element].parent_id;
                                        item.attr_id[parent_id] = parent_id;
                                    });
                                }
                                return item
                            });
                            this.priceAttribute.index = this.priceAttribute.data.length - 1;
                            this.priceAttribute._onmouseout(true);
                            this.priceAttribute.submit_flags = true
                        }, 100);
                    } else {
                        this.priceAttribute.index = -1;
                        this.priceAttribute.add();
                    }
                    break;

                case "getlistproductgroup":
                    this.data.group = res.data;
                    break;

                case 'getlistbrand':
                    this.data.brand = res.data;
                    break;

                case 'getlistOrigin':
                    this.data.origin = res.data;
                    break;

                case "getlistAttribute":
                    data = res.data;
                    this.attribute.list = data.reduce((n, o, i) => { n[o.id] = o; return n }, []);
                    this.attribute.data = this.attribute._ini(res.data);
                    break;

                case "getlistdocument":
                    this.document.data = this.document._ini(res.data);
                    if (this.id > 0) {
                        this.globals.send({ path: this.token.getDocument, token: "getDocument", params: { id: this.id } });
                    }
                    break;

                case "getDocument":
                    let dataList = this.document._ini(res.data);
                    this.document._concat(dataList);
                    setTimeout(() => {
                        if (window['tooltip']) {
                            window['tooltip']();
                        }
                    }, 500);
                    break;

                case 'processProduct':
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    let id = res.data;

                    if (this.id === 0) {
                        this.id = +id;
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/products/update/' + this.id]);
                        }, 100);
                    } else {
                        this.flags = true;
                    }
                    break;

                case 'updateDocument':
                case 'syncDocument':
                case 'updateDevice':
                case 'updateSeo':
                case 'updatePrice':
                case 'updateImages':
                case 'updateProduct':
                case 'updateAttribute':
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    if (res.token == 'updateDocument' || res.token == 'syncDocument') {
                        this.document.data = this.document._ini(this.document.data);
                        this.globals.send({ path: this.token.getDocument, token: "getDocument", params: { id: this.id } });
                    }
                    break;
                case 'updatePriceAttribute':
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    this.get();
                    break;
                case "removePriceAttribute":
                    this.toastr[type](res.message, type, { timeOut: 1500 });
                    this.get();
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getlistDocument, token: 'getlistdocument' })
        this.globals.send({ path: this.token.getlistBrand, token: 'getlistbrand' });
        this.globals.send({ path: this.token.getlistOrigin, token: 'getlistOrigin' });
        this.globals.send({ path: this.token.getlistAttribute, token: "getlistAttribute" });
        this.globals.send({ path: this.token.getlistproductgroup, token: 'getlistproductgroup', params: { type: 3 } });
        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
            if (this.id && this.id > 0) {
                this.get();
            } else {
                this.fmConfigs();
                this.priceConfigs();
                this.seoConfigs();
                this.productImages.set();
                this.attribute.data && this.attribute.data.length > 0 ? this.priceAttribute.add() : ''
            }
        });
    }

    get() {
        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    //Cấu hình thông tin sản phẩm
    onChangeLink(e) {
        const url = this.link._convent(e.target.value);
        this.fm.value.link = url;
    }

    fmConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { status: 1, page_id: 0, origin_id: 0, brand_id: 0 };


        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            link: [item.link ? item.link : '', [Validators.required]],
            page_id: [item.page_id ? item.page_id : 0, [Validators.required]],
            brand_id: [item.brand_id ? item.brand_id : 0, [Validators.required]],
            origin_id: [item.origin_id ? item.origin_id : 0, [Validators.required]],
            info: item.info ? item.info : '',
            hot: +item.hot ? +item.hot : 0,
            detail: item.detail ? item.detail : '',
            status: (item.status && +item.status === 1) ? true : false,
        });
        this.device.value = (item.device ? item.device : '');
        if (this.data.group && this.data.group.length > 0 && this.fm.value.page_id > 0) {
            this.group.item = this.data.group.find(x => +x.id === +this.fm.value.page_id) || {};
            this.group.name = Object.keys(this.group.item).length > 0 ? this.group.item.name : '';
            if (Object.keys(this.group.item).length == 0) {
                this.fm.controls['page_id'].setValue(0);
            }
        }
    }
    onSubmit() {
        if (this.fm.valid && this.flags) {
            this.flags = false;
            const obj = this.fm.value;
            this.globals.send({ path: this.token.process, token: 'processProduct', data: obj, params: { id: this.id || 0 } });
        }
    }
    // thuộc tính 
    public attribute = {
        data: <any>[],
        list: [],
        _ini: (data = []) => {
            let list = [];
            data = data.filter(function (item: { parent_id: string | number; }) {
                let v = (isNaN(+item.parent_id) && item.parent_id) ? 0 : +item.parent_id;
                v == 0 ? '' : list.push(item);
                return v == 0 ? true : false;
            })
            let compaidmenu = (data: string | any[], skip: boolean) => {
                if (skip == true) {
                    return data;
                } else {
                    for (let i = 0; i < data.length; i++) {
                        let obj = data[i]['data'] && data[i]['data'].length > 0 ? data[i]['data'] : []
                        list = list.filter(item => {
                            let skip = (+item.parent_id == +data[i]['id']) ? false : true;
                            if (skip == false) { obj.push(item); }
                            return skip;
                        })
                        let skip = (obj.length == 0) ? true : false;
                        data[i]['data'] = compaidmenu(obj, skip);
                    }
                    return data;
                }
            };
            return compaidmenu(data, false);;
        },
    }

    //Cấu hình giá
    formatPrice = (price) => {
        return +price.toString().replace(/\./g, '');
    }

    priceConfigs(item: any = "") {
        item = typeof item === 'object' ? item : { inventory: 1 };
        this.price = this.fb.group({
            price_sale: [item.price_sale ? +item.price_sale : 0],
            inventory: item.inventory ? +item.inventory : 1,
            price: [item.price ? +item.price : 0],
            vat: item.vat ? +item.vat : 0,
        });
    }
    updatePrice = () => {
        const obj = this.price.value;
        obj.price = this.formatPrice(obj.price);
        obj.price_sale = this.formatPrice(obj.price_sale);
        this.globals.send({ path: this.token.updatePrice, token: 'updatePrice', data: obj, params: { id: this.id || 0 } });
    }

    public priceAttribute = {
        token: 'set/product/update/priceattribute',
        tokenDelete: 'set/product/remove/priceattribute',
        data: [],
        index: -1,
        flags: true,
        onmouseout: false,
        submit_flags: true,
        _onmouseout: (e) => {
            this.priceAttribute.onmouseout = true;
            this.priceAttribute.flags = this.formatPrice(this.priceAttribute.data[this.priceAttribute.index].price) > 0 ? false : true;
        },
        _checkAttr: (attr_id) => {
            return this.priceAttribute.data.length > 0 && this.priceAttribute.data[this.priceAttribute.index].attr_id[attr_id] ? true : false;
        },
        _push: (rows) => {
            this.priceAttribute.data[this.priceAttribute.index].attr_id[rows.parent_id] = rows.parent_id;
            this.priceAttribute.data[this.priceAttribute.index].att_id_value.push(rows.id)
            this.priceAttribute.data[this.priceAttribute.index].list.push(rows);
        },
        remove(item, attr_id, id, index) {
            delete item.attr_id[attr_id];
            if (+item.att_id_value.indexOf(id) > -1) {
                item.att_id_value.splice(+item.att_id_value.indexOf(id), 1);
            }
            item.list.splice(index, 1);
        },
        add: () => {

            this.priceAttribute.submit_flags = true
            this.priceAttribute.onmouseout = false;
            this.priceAttribute.flags = true;

            let item = { price: 0, list: [], attr_id: [], att_id_value: [], status: 0, images: new uploadFileService() };

            const imagesConfig = { path: this.globals.BASE_API_URL + 'public/products/', };

            item.images._ini(imagesConfig);

            if (this.priceAttribute.index == -1) {

                this.priceAttribute.index = this.priceAttribute.index + 1;

                this.priceAttribute.data[this.priceAttribute.index] = item;

            } else {

                if (this.formatPrice(this.priceAttribute.data[this.priceAttribute.index].price) > 0) {

                    this.priceAttribute.index = this.priceAttribute.index + 1;

                    this.priceAttribute.data[this.priceAttribute.index] = item
                }
            }
        },
        _delete: (id, index) => {
            this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'products.removePriceAttribute', name: index } });
            this.modalRef.content.onClose.subscribe(result => {
                if (result == true && id > 0) {
                    this.globals.send({ path: this.priceAttribute.tokenDelete, token: 'removePriceAttribute', params: { id: id } });
                }
            });

        },
        _get: () => {
            let data = [];
            if (this.priceAttribute.data.length > 0) {
                this.priceAttribute.data.filter(item => {
                    let row: any = {}
                    if (item.att_id_value.length > 0) {
                        row.att_id_value = JSON.stringify(item.att_id_value);
                        row.price = this.formatPrice(item.price);
                        row.id = +item.id || 0;
                        row.images = item.images._get(true);
                        data.push(row)
                    }
                });
            }
            return data || []
        },
        _onSubmit: () => {
            let data = this.priceAttribute._get();
            if (data && data.length > 0 && this.priceAttribute.submit_flags == true) {
                this.priceAttribute.submit_flags = false
                this.globals.send({ path: this.priceAttribute.token, token: 'updatePriceAttribute', data: data, params: { product_id: +this.id || 0 } });
            }
        }
    }

    //Cấu hình SEO
    seoConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { status: 1 };

        this.seo = this.fb.group({

            description: item.description ? item.description : '',

        });

        this.tags._set(item.keywords ? JSON.parse(item['keywords']) : "");
    }

    updateSeo = () => {

        const obj = this.seo.value;

        obj.keywords = this.tags._get();

        this.globals.send({ path: this.token.updateSeo, token: 'updateSeo', data: obj, params: { id: this.id || 0 } });
    }

    //Cấu hình hình và chi tiết hình
    public productImages = {
        show: false,
        tokenProcess: 'set/product/updateImages',
        set: (item: any = '') => {

            item = typeof item === 'object' ? item : { images: '', listimages: '' };

            const imagesConfig = { path: this.globals.BASE_API_URL + 'public/products/', data: item.images ? item.images : '' };

            this.images._ini(imagesConfig);

            const listimagesConfig = { path: this.globals.BASE_API_URL + 'public/products/', data: item.listimages ? item.listimages : '', multiple: true };

            this.listimages._ini(listimagesConfig);
        },

        process: () => {

            let data: any = {};

            data.images = this.images._get(true);

            data.listimages = this.listimages._get(true);

            this.globals.send({ path: this.productImages.tokenProcess, token: 'updateImages', data: data, params: { id: this.id > 0 ? this.id : 0 } });

        }
    }

    //Cấu hình tài liệu sản phẩm
    public document = {
        token: {
            update: 'set/product/updateDocument',
            sync: 'set/product/syncDocument'
        },
        data: [],
        new: [],
        old: [],
        search: '',
        field: ['name'],
        show: false,
        _ini: (data) => {
            for (let i = 0; i < data.length; i++) {
                data[i].sync = false;
            }
            return data;
        },
        _concat: (data) => {
            this.document.old = [];
            this.document.new = data;
            let tamp = data.reduce((n, o) => {
                n[o.id] = o;
                this.document.old.push(o);
                return n;
            }, []);
            this.document.data = this.document.data.filter((res: any) => !tamp[res.id]);
        },
        _add: (item) => {
            item.sync = (!item.sync) ? true : false;
            this.document.data = this.document.data.filter((res: any) => res.name != item.name);
            this.document.new.push(item);
            setTimeout(() => {
                if (window['tooltip']) {
                    window['tooltip']();
                }
            }, 500);
        },
        _remove: (item) => {
            item.sync = (!item.sync) ? true : false;
            this.document.new = this.document.new.filter((res: any) => res.name != item.name);
            this.document.data.push(item);
            setTimeout(() => {
                if (window['tooltip']) {
                    window['tooltip']();
                }
            }, 500);
        },
        _reduce: (data) => {
            return data.reduce((n, o) => { n[o.id] = o; return n; }, []);
        },
        _sync: (item, type) => {

            this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'products.messageSync', name: item.name } });

            this.modalRef.content.onClose.subscribe(result => {

                if (result == true) {

                    this.globals.send({ path: this.document.token.sync, token: 'syncDocument', params: { product_id: this.id, document_id: item.id, type: type } })
                }
            });
        },
        _get: () => {

            let old = this.document._reduce(this.document.old);
            let add = this.document.new.filter((item) => !old[item.id]);

            let result = { add: [], remove: [] };
            for (let i = 0; i < add.length; i++) {
                let item = add[i];
                result.add.push({
                    id: 0,
                    product_id: this.id,
                    document_id: +item.id,
                    keywords: JSON.stringify(item.keywords),
                    sync: item.sync,
                    status: 1
                })
            }
            add = this.document._reduce(result.add);
            let news = this.document._reduce(this.document.new);
            result.remove = this.document.old.filter((item) => !news[item.id] && !add[+item.id]);

            return result;
        },
        _submit: () => {
            let data = this.document._get();

            this.globals.send({ path: this.document.token.update, token: "updateDocument", data: data, params: { product_id: this.id } });
        }
    }

    // Cấu hình file cài đặt hệ thống cho san phẩm (đang khoá)
    public device = {
        token: 'set/product/updateDevice',
        value: '',
        show: false,
        _submit: () => {
            let data = { device: this.device.value };
            this.globals.send({ path: this.device.token, token: "updateDevice", data: data, params: { id: this.id || 0 }, });
        }
    }
}