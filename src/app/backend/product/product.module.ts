import { NgModule } from "@angular/core";

import { CommonModule } from "@angular/common";
import { TranslateModule } from "@ngx-translate/core";
import { Routes, RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { TooltipModule } from "ngx-bootstrap/tooltip";

import { ServicePipeModule } from '../../services/pipe';
import { CKEditorModule } from "ckeditor4-angular";

import { GetlistComponent } from "./getlist/getlist.component";
import { GroupProductGetlistComponent } from "./group/group-product.component";
import { MainComponent } from "./main/main.component";
import { ProcessProductComponent } from "./process/process.component";
import { numberModule } from "../../services/symbol";

const appRoutes: Routes = [
    { path: "", redirectTo: "get-list" },
    { path: "get-list", component: MainComponent },
    { path: "insert", component: ProcessProductComponent },
    { path: "update/:id", component: ProcessProductComponent },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(appRoutes),
        TooltipModule.forRoot(),
        TranslateModule,

        CKEditorModule,
        ServicePipeModule,
        numberModule
    ],
    declarations: [
        GetlistComponent,
        GroupProductGetlistComponent,
        ProcessProductComponent,
        MainComponent
    ],
})
export class ProductModule { }
