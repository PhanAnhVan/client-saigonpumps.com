import { Component, OnInit, TemplateRef, ViewContainerRef, OnDestroy, Input, SimpleChanges } from '@angular/core';
import { TableService } from '../../../services/integrated/table.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals'
import { AlertComponent } from '../../modules/alert/alert.component';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
    styleUrls: ['./getlist.component.css']
})
export class GetlistComponent implements OnInit {


    private id: number;

    modalRef: BsModalRef;

    public connect;

    @Input("filter") filter: any;

    @Input("change") change: any;

    public token: any = {

        getlist: "get/evaluate/getlist",
        remove: "set/evaluate/remove",
        changeType: "set/evaluate/changetype",

    }

    private cols = [
        { title: 'lblStt', field: 'index', show: true, filter: true },
        { title: 'lblImages', field: 'logo', show: true, filter: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true },
        { title: '', field: 'status', show: true, filter: true },
    ];

    public cstable = new TableService();

    constructor(

        public globals: Globals,

        private modalService: BsModalService,

        public toastr: ToastrService,
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "getlistevaluate":

                    this.cstable._concat(res.data, true);

                    break;
                case "changeType":
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type, { closeButton: true });

                    this.globals.send({ path: this.token.getlist, token: 'getlistevaluate' });

                    break;

                case "removeevaluate":

                    let status = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");

                    this.toastr[status](res.message, status, { closeButton: true });

                    if (res.status == 1) {

                        this.globals.send({ path: this.token.getlist, token: 'getlistevaluate' });
                    }
                    break
                default:
                    break;
            }
        });



    }

    ngOnInit() {

        this.globals.send({ path: this.token.getlist, token: 'getlistevaluate' });

        this.cstable._ini({ cols: this.cols, data: [], keyword: 'evaluate', count: 25 });

    }

    ngOnDestroy() {

        this.connect.unsubscribe();

    }

    ngOnChanges(e: SimpleChanges) {

        if (typeof this.filter === 'object') {

            let value = Object.keys(this.filter);

            if (value.length == 0) {

                this.cstable._delFilter('page_id');

            } else {
                this.cstable._setFilter('page_id', value, 'in', 'number');
            }
        }
    }
    changeType = (type, id) => {

        type = type == 1 ? 0 : 1;

        this.globals.send({ path: this.token.changeType, token: 'changeType', params: { id: id, type: type } });

    }
    onRemove(id: number, name: any) {

        this.id = id;

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'evaluate.remove', name: name } });

        this.modalRef.content.onClose.subscribe(result => {

            if (result == true) {

                this.globals.send({ path: this.token.remove, token: 'removeevaluate', params: { id: id || 0 } });
            }
        });
    }
}
