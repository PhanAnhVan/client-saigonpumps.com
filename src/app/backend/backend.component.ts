import { Component, AfterViewInit } from '@angular/core';
import { Globals } from '../globals';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-backend',
    templateUrl: './backend.component.html',
})
export class BackendComponent implements AfterViewInit {
    public opened: boolean = true;
    public height: number = window.innerHeight;

    constructor(
        public globals: Globals,
        public translate: TranslateService
    ) {
        this.translate.use('admin');
        if (window['tooltip']) {
            window['tooltip']();
        }

        let src = 'https://homeq.vn/public/ckeditor/ckeditor.js';
        if (!document.querySelector("[src='" + src + "']")) {
            let el = document.createElement("script");
            el.setAttribute("type", "text/javascript");
            el.setAttribute("ckeditor", "ckeditor");
            el.setAttribute('src', src);
            document.body.appendChild(el);
        }
    }
    eventOpened(e: any) {
        this.opened = (this.opened == true) ? false : true;
    }
    ngAfterViewInit() {
        setTimeout(() => {
            let el = document.getElementById('facebook');
            if (el) { el.remove() }
        }, 1000);
    }

}
