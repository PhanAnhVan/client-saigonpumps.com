import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { Globals } from "../../globals";
import { Observable } from 'rxjs';
@Injectable()
export class UserAuthGuard implements CanActivate {
    constructor(private router: Router, private globals: Globals) { }
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.globals.CUSTOMER.check()) {
            let skip = true
            this.globals.CUSTOMER._check().subscribe((res: any) => {
                skip = res.skip
                if (res.skip == false) {
                    this.globals.CUSTOMER.remove(true);
                    this.router.navigate(['/']);
                    return false;
                } else {
                    this.globals.CUSTOMER.set(res.data);
                    return true;
                }
            });
            return skip;
        }
        else {
            this.router.navigate(['/']);
            return false;
        }
    }
}
